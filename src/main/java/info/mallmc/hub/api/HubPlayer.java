package info.mallmc.hub.api;

import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.hub.Hub;
import info.mallmc.hub.api.shop.IShop;
import info.mallmc.hub.shops.Shops;
import info.mallmc.hub.shops.clothes.ClothesProduct;
import info.mallmc.hub.shops.clothes.ClothesShop;
import info.mallmc.hub.shops.clothes.ClothingType;
import info.mallmc.hub.util.HotbarItems;
import info.mallmc.hub.util.HubPlayerData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import org.bson.types.ObjectId;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.IndexOptions;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.PostLoad;
import org.mongodb.morphia.annotations.PrePersist;
import org.mongodb.morphia.annotations.Transient;

@Entity("hubPlayers")
public class HubPlayer {

  private static HashMap<UUID, HubPlayer> players = new HashMap<>();

  public static HashMap<UUID, HubPlayer> getPlayers() {
    return players;
  }

  /**
   * Get a player from the current list of players.
   *
   * @param uuid The uuid of the player to get
   * @return The player from the list
   */
  public static HubPlayer getPlayer(UUID uuid) {
    if (!players.containsKey(uuid)) {
      return null;
    }
    return players.get(uuid);
  }

  /**
   * Add a player to the list or create a player
   *
   * @param uuid The UUID of the player to create / add
   * @param username The username of the player to create / add
   * @return The player that was created / added
   */
  public static HubPlayer createPlayer(UUID uuid, String username) {
    if (!players.containsKey(uuid)) {
      if (HubPlayerData.getInstance().isInDatabase(uuid)) {
        HubPlayer mallPlayer = HubPlayerData.getInstance().getPlayerByUUID(uuid);
        players.put(uuid, mallPlayer);
      } else {
        HubPlayer mallPlayer = new HubPlayer(uuid);
        players.put(uuid, mallPlayer);
        return mallPlayer;
      }
    } else {
      return getPlayer(uuid);
    }
    return null;
  }

  public static void removePlayer(UUID uuid) {
    if (players.containsKey(uuid)) {
      players.remove(uuid);
    }
  }

  @Id
  private ObjectId id;
  @Indexed(options = @IndexOptions(unique = true))
  private UUID uuid;
  @Transient
  private HashMap<ClothingType, List<ClothesProduct>> unlockedClotheProducts;
  @Transient
  private HashMap<ClothingType, ClothesProduct> equippedClothes;
  private HashMap<ClothingType, List<Integer>> unlockedClotheProduct;
  private HashMap<ClothingType, Integer> equippedClothe;
  private HashMap<Integer, HotbarItems> hotbarItemsHashMap;
  private PlayerVisibility playerVisibility;

  public HubPlayer(UUID uuid) {
    this.uuid = uuid;
    this.unlockedClotheProduct = new HashMap<>();
    this.equippedClothe = new HashMap<>();
    this.hotbarItemsHashMap = new HashMap<>();
    resetHotbar();
    playerVisibility = PlayerVisibility.VIEW_ALL;
  }

  @PrePersist
  void prePersist() {
//    this.unlockedClotheProduct.clear();
//    this.equippedClothe.clear();
    HashMap<ClothingType, List<Integer>> unlocked = new HashMap<>();
    HashMap<ClothingType, Integer> equipped = new HashMap<>();
    if(this.unlockedClotheProducts == null){
      this.unlockedClotheProducts = new HashMap<>();
    }
    for(ClothingType type : this.unlockedClotheProducts.keySet()){
      List<Integer> list = new ArrayList<>();
      for(ClothesProduct product : this.unlockedClotheProducts.get(type)){
        list.add(product.getId());
      }
      unlocked.put(type, list);
    }
    if(this.equippedClothes == null){
      this.equippedClothes = new HashMap<>();
    }
    for(ClothingType type : this.equippedClothes.keySet()) {
      equipped.put(type, this.equippedClothes.get(type).getId());
    }
    this.unlockedClotheProduct = unlocked;
    this.equippedClothe = equipped;
  }

  @PostLoad
  void postLoad(){
    this.unlockedClotheProducts = new HashMap<>();
    this.equippedClothes = new HashMap<>();
    IShop shop = Shops.getRegisteredShops().get("Clothes");
    ClothesShop clothesShop = (ClothesShop) shop;
    if(this.unlockedClotheProduct == null){
      this.unlockedClotheProduct = new HashMap<>();
    }
    for(ClothingType type : this.unlockedClotheProduct.keySet()){
      this.unlockedClotheProducts.put(type, new ArrayList<>());
      for(int i : this.unlockedClotheProduct.get(type)){
        this.unlockedClotheProducts.get(type).add(clothesShop.getClothesProducts().get(type).get(i));
      }
    }
    if(this.equippedClothe == null){
      this.equippedClothe = new HashMap<>();
    }
    for(ClothingType type : this.equippedClothe.keySet()){
      this.equippedClothes.put(type, clothesShop.getClothesProducts().get(type).get(this.equippedClothe.get(type)));
    }
  }

  public HubPlayer() {
  }

  public UUID getUuid() {
    return uuid;
  }

  public ObjectId getId() {
    return id;
  }

  public void setId(ObjectId id) {
    this.id = id;
  }


  public HashMap<ClothingType, List<ClothesProduct>> getUnlockedClotheProducts() {
    return unlockedClotheProducts;
  }

  public void setUnlockedClotheProducts(
      HashMap<ClothingType, List<ClothesProduct>> unlockedClotheProducts) {
    this.unlockedClotheProducts = unlockedClotheProducts;
  }

  public HashMap<ClothingType, ClothesProduct> getEquippedClothes() {
    return equippedClothes;
  }

  public void setEquippedClothes(
      HashMap<ClothingType, ClothesProduct> equippedClothes) {
    this.equippedClothes = equippedClothes;
  }

  public HashMap<Integer, HotbarItems> getHotbarItemsHashMap() {
    return hotbarItemsHashMap;
  }

  public void resetHotbar(){
    hotbarItemsHashMap.clear();
    hotbarItemsHashMap.put(0, HotbarItems.NAVIGATION);
    hotbarItemsHashMap.put(1, HotbarItems.PLAYER_INFO);
  }

  public void setHotbarItemsHashMap(
      HashMap<Integer, HotbarItems> hotbarItemsHashMap) {
    this.hotbarItemsHashMap = hotbarItemsHashMap;
  }

  public PlayerVisibility getPlayerVisibility() {
    return playerVisibility;
  }

  public void setPlayerVisibility(PlayerVisibility playerVisibility) {
    this.playerVisibility = playerVisibility;
  }

  public void sortPlayerVisibility(){
    Player player = Hub.getInstance().getServer().getPlayer(uuid);
    MallPlayer mallPlayer = MallPlayer.getPlayer(uuid);
    switch(playerVisibility){
      case VIEW_ALL:
        for(Player player1 : Bukkit.getOnlinePlayers()){
          player.showPlayer(player1);
        }
        break;
      case VIEW_FRIENDS:
        for(Player player1 : Bukkit.getOnlinePlayers()){
          if(mallPlayer.getFriends() != null && !mallPlayer.getFriends().contains(player1.getUniqueId())){
            player.hidePlayer(player1);
          }else{
            player.showPlayer(player1);
          }
        }
        break;
      case NONE:
        for(Player player1 : Bukkit.getOnlinePlayers()){
          player.hidePlayer(player1);
        }
        break;
    }
  }

  public enum PlayerVisibility {
    VIEW_ALL,
    VIEW_FRIENDS,
    NONE
  }
}
