package info.mallmc.hub.api.shop;

import info.mallmc.core.MallCore;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.framework.util.Messaging;
import java.util.List;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public interface IProduct {

  String getName();

  default String getDisplayName(MallPlayer mallPlayer) {
    return Messaging.getMessage(mallPlayer.getLanguageIdentifier(), "hub.product." + this.getName() + ".name");
  }

  default List<String> getLore(MallPlayer mallPlayer)
  {
    List<String> strings =  MallCore.getInstance().getLanguageMap().get(mallPlayer.getLanguageIdentifier()).getStrings("hub.product." + this.getName() +".lore");
    for(String string : strings){
      strings.set(strings.indexOf(string), Messaging.colorizeMessage(string));
    }
    return strings;
  }

  int getCost();

  void onInteract(PlayerInteractEvent playerInteractEvent);

  ItemStack getIcon();

  IShop getShop();

}
