package info.mallmc.hub.api.shop;

import info.mallmc.framework.util.jukebox.MallJukebox;
import info.mallmc.hub.shops.Shops;
import org.bukkit.entity.Player;

import javax.annotation.Nullable;
import java.util.List;

public interface IShop {

  ShopArea getShopArea();

  String getName();

  boolean hasMusic();

  EnumShopState getShopState();

  boolean setShopState(EnumShopState enumShopState);

  default String getMusicURL() {
    return "http://cdn.redstone.tech/Rushmeadfiles/mallmc/mcjukebox/hub/" + getName().toLowerCase() + "/" + getName().toLowerCase() + "1.mp3";
  }

  ShopDoor getDoor();

  void registerInventories();

  void registerInteractions();

  void registerEntities();

  void onPlayerEnter(Player player);

  void onPlayerLeave(Player player);

  void onEnable();

  void onDisable();

  default void onRegister(){
    setShopState(EnumShopState.OPEN);
    registerInventories();
    registerEntities();
    registerInteractions();
  };

  void purchaseAction(Player player, IProduct product);

  List<IProduct> getProducts();

  @Nullable
  default MallJukebox getJukebox() {
    return Shops.getShopJuekbox(this);
  }


}
