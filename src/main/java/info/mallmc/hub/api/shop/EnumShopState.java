package info.mallmc.hub.api.shop;

public enum EnumShopState {
    OPEN(true),
    CLOSE(false);


    private boolean open;

    EnumShopState(boolean open) {
        this.open = open;
    }

    public boolean isOpen() {
        return open;
    }
}
