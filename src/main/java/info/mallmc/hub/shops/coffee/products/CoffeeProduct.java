package info.mallmc.hub.shops.coffee.products;

import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.framework.util.helpers.SoundHelper;
import info.mallmc.framework.util.letters.ParticleEffect;
import info.mallmc.hub.Hub;
import info.mallmc.hub.api.shop.IProduct;
import info.mallmc.hub.api.shop.IShop;
import info.mallmc.hub.util.HubItems;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class CoffeeProduct implements IProduct {

  private String name;
  private int cost;
  private ItemStack itemStack;
  private IShop shop;
  private boolean givesCup;

  public CoffeeProduct(String name, int cost, ItemStack itemStack, IShop shop, boolean givesCup) {
    this.name = name;
    this.cost = cost;
    this.itemStack = itemStack;
    this.shop = shop;
    this.givesCup = givesCup;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public int getCost() {
    return cost;
  }

  @Override
  public void onInteract(PlayerInteractEvent playerInteractEvent) {
    Player player = playerInteractEvent.getPlayer();
    if(player.isSneaking()){
      player.setSneaking(false);
      SoundHelper.playSoundAtPlayer(player, Sound.ENTITY_GENERIC_DRINK);
      Hub.getInstance().getServer().getScheduler().runTaskLaterAsynchronously(Hub.getInstance(), () -> {
        Hub.getInstance().getServer().getScheduler().runTaskLaterAsynchronously(Hub.getInstance(), () -> { SoundHelper.playSoundAtPlayer(player, Sound.ENTITY_PLAYER_BURP); }, 20 *2);
        ParticleEffect.WATER_SPLASH.display(0, 1, 0, 10, 50, player.getLocation(), 200D);
        MallPlayer mallPlayer = MallPlayer.getPlayer(player.getUniqueId());
        mallPlayer.setXp(mallPlayer.getXp() + 10);
        player.getInventory().remove(playerInteractEvent.getItem());
        if(shouldGiveCup()){
          player.getInventory().setItemInMainHand(HubItems.getEmptyCup(mallPlayer));
        }
        }, 20);
    }
  }

  @Override
  public ItemStack getIcon() {
    return itemStack;
  }

  @Override
  public IShop getShop() {
    return shop;
  }

  private boolean shouldGiveCup(){
    return givesCup;
  }
}
