package info.mallmc.hub.shops.coffee;

import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.framework.entities.VillagerNPC;
import info.mallmc.framework.interactions.Interaction;
import info.mallmc.framework.interactions.InteractionRegistry;
import info.mallmc.framework.util.Messaging;
import info.mallmc.framework.util.items.ItemUtil;
import info.mallmc.framework.util.registies.EntityRegistry;
import info.mallmc.hub.Hub;
import info.mallmc.hub.api.shop.EnumShopState;
import info.mallmc.hub.api.shop.IProduct;
import info.mallmc.hub.api.shop.IShop;
import info.mallmc.hub.api.shop.ShopArea;
import info.mallmc.hub.api.shop.ShopDoor;
import info.mallmc.hub.api.shop.ShopDoor.DoorType;
import info.mallmc.hub.shops.ShopInventory;
import info.mallmc.hub.shops.coffee.products.CakeProduct;
import info.mallmc.hub.shops.coffee.products.CoffeeProduct;
import java.util.Arrays;
import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class CoffeeShop implements IShop {

  private VillagerNPC kevinNPC;
  private Location moveToBlock;
  private EnumShopState shopState = null;

  @Override
  public ShopArea getShopArea() {
    return new ShopArea(new Location(Hub.getInstance().getHubWorld(), 49, 64, -26), new Location(Hub.getInstance().getHubWorld(), 64, 61, -11));
  }

  @Override
  public String getName() {
    return "CraftyCoffee";
  }

  @Override
  public boolean hasMusic() {
    return false;
  }

  @Override
  public EnumShopState getShopState() {
    return shopState;
  }

  @Override
  public boolean setShopState(EnumShopState enumShopState) {
    if (this.shopState == enumShopState) {
      return false;
    } else {
      this.shopState = enumShopState;
      if(shopState.isOpen()) {
        getDoor().openDoor();
        return true;
      }else {
        getDoor().closeDoor();
        return true;
      }
    }
  }

  @Override
  public ShopDoor getDoor() {
      return new ShopDoor(new Location(Hub.getInstance().getHubWorld(), 58, 63, -27), new Location(Hub.getInstance().getHubWorld(), 55, 61, -27), (byte) 1, DoorType.UP_AND_DOWN);
  }

  @Override
  public void registerInventories() {
  }

  @Override
  public void registerInteractions() {
    InteractionRegistry.registerInteraction(Interaction.RIGHT_CLICK_ENTITY, this::clickEntity);
  }

  private void clickEntity(Event event){
    PlayerInteractEntityEvent playerInteractEntityEvent = (PlayerInteractEntityEvent) event;
    if(playerInteractEntityEvent.getRightClicked().getUniqueId() == kevinNPC.getUniqueID()){
      playerInteractEntityEvent.setCancelled(true);
      new ShopInventory(MallPlayer.getPlayer(playerInteractEntityEvent.getPlayer().getUniqueId()), "hub.inventories.kevin.name", this, false, playerInteractEntityEvent.getPlayer()).setVillagerNPC(kevinNPC).open(playerInteractEntityEvent.getPlayer());
    }
  }

  @Override
  public void registerEntities() {
    kevinNPC = EntityRegistry
            .registerVillager(new Location(Hub.getInstance().getHubWorld(), 61, 61, -19), "&3&lKevin");
    moveToBlock = new Location(Hub.getInstance().getHubWorld(), 63, 61, -15, -90, 9);
  }

  @Override
  public void onPlayerEnter(Player player) {

  }

  @Override
  public void onPlayerLeave(Player player) {

  }

  @Override
  public void onEnable() {
  }

  @Override
  public void onDisable() {

  }

  @Override
  public void onRegister() {
    setShopState(EnumShopState.OPEN);
    registerInventories();
    registerEntities();
    registerInteractions();
  }

  @Override
  public void purchaseAction(Player player, IProduct product) {
    MallPlayer mallPlayer = MallPlayer.getPlayer(player.getUniqueId());
    kevinNPC.moveToBlock(moveToBlock, 0.7F);
    kevinNPC.setBusy(true);
    Hub.getInstance().getServer().getScheduler()
            .runTaskLaterAsynchronously(Hub.getInstance(), () -> {
              kevinNPC.moveToBlock(kevinNPC.getSpawnLocation(), 0.7F);
              Hub.getInstance().getServer().getScheduler()
                      .runTaskLaterAsynchronously(Hub.getInstance(), () -> {
                        ItemStack itemStack = product.getIcon();
                        ItemMeta itemMeta = itemStack.getItemMeta();
                        itemMeta.setDisplayName(product.getDisplayName(mallPlayer));
                        List<String> newLore = product.getLore(mallPlayer);
                        itemMeta.setLore(newLore);
                        itemStack.setItemMeta(itemMeta);
                        player.getInventory().addItem(itemStack);
                        kevinNPC.setBusy(false);
                        Messaging
                                .sendRawMessage(player, "hub.shops.npc.ordergot", kevinNPC.getName(), player.getName(), itemStack.getItemMeta().getDisplayName());
                      }, 20 * 2);
            }, 20 * 10);
  }

  @Override
  public List<IProduct> getProducts() {
    CoffeeProduct americano = new CoffeeProduct("americano", 2, ItemUtil.getSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZTFjZWRkNTdlYzFiM2FjMTQ1NDQ2MjZjYzZiNGJjZGJkYzM1MTNmMzlhOTFjYzM3YTA0OGE5ZmQyNDRkNGQifX19", "hub.products.americano.name", Arrays.asList("hub.products.american.lore")), this, true);
    CoffeeProduct latte = new CoffeeProduct("latte", 3, ItemUtil.getSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvN2VhMGY3NzU3MTg1YmU5ZGY1YjJmYzlkODVkNDA2NDJlYTRmZGI0NTE1ZjMxNGRhMThmNTljNjk2ZTViZTkifX19", "hub.products.latte.name", Arrays.asList("hub.products.latte.lore")), this, true);
    CoffeeProduct iced = new CoffeeProduct("iced", 4, ItemUtil.getSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTQxYTY5ZTE2NmMzYmI1ZGI4OWUyNzQzZDczZGE1Y2QwNjE5ZGE1ZTJlOTIzZGE5OWMyZTU1YmE4NTNkOSJ9fX0=", "hub.products.iced.name", Arrays.asList("hub.products.iced.lore")), this, true);
    CakeProduct coffeeCake = new CakeProduct("coffeecake", 4, ItemUtil.getSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZWM2ZWI4ZjE1YmEwZDc5OTNiZjg3MDhmYTFkZDg2YzFlOGZkZTc0MWE3ZGRlOTE5NWYyMjg5MWUwMjE1MyJ9fX0=", "hub.products.coffeecake.name", Arrays.asList("hub.products.coffeecake.lore")), this);
    CakeProduct sprinkleCake = new CakeProduct("sprinklecake", 4, ItemUtil.getSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZjkxMzY1MTRmMzQyZTdjNTIwOGExNDIyNTA2YTg2NjE1OGVmODRkMmIyNDkyMjAxMzllOGJmNjAzMmUxOTMifX19", "hub.products.sprinklecake.name", Arrays.asList("hub.products.sprinklecake.lore")), this);
    return Arrays.asList(americano, latte, iced, coffeeCake, sprinkleCake);
  }
}
