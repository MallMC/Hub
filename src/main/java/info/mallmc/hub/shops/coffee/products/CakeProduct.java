package info.mallmc.hub.shops.coffee.products;

import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.framework.util.helpers.SoundHelper;
import info.mallmc.hub.Hub;
import info.mallmc.hub.api.shop.IProduct;
import info.mallmc.hub.api.shop.IShop;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityStatus;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class CakeProduct implements IProduct {

  private String name;
  private int cost;
  private ItemStack itemStack;
  private IShop shop;

  public CakeProduct(String name, int cost, ItemStack itemStack, IShop shop) {
    this.name = name;
    this.cost = cost;
    this.itemStack = itemStack;
    this.shop = shop;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public int getCost() {
    return cost;
  }



  @Override
  public void onInteract(PlayerInteractEvent playerInteractEvent) {
    Player player = playerInteractEvent.getPlayer();
    if(player.isSneaking()){
      player.setSneaking(false);
      SoundHelper.playSoundAtPlayer(player, Sound.ENTITY_GENERIC_EAT);
      Hub.getInstance().getServer().getScheduler().runTaskLaterAsynchronously(Hub.getInstance(), () -> {
        Hub.getInstance().getServer().getScheduler().runTaskLaterAsynchronously(Hub.getInstance(), () -> { SoundHelper.playSoundAtPlayer(player, Sound.ENTITY_PLAYER_BURP); }, 20 *2);
        CraftPlayer cPlayer = ((CraftPlayer)player);
        PacketPlayOutEntityStatus eatPacket = new PacketPlayOutEntityStatus(cPlayer.getHandle(), (byte)9);
        cPlayer.getHandle().playerConnection.sendPacket(eatPacket);
        MallPlayer mallPlayer = MallPlayer.getPlayer(player.getUniqueId());
        mallPlayer.setXp(mallPlayer.getXp() + 5);
        player.getInventory().remove(playerInteractEvent.getItem());
      }, 20);
    }
  }

  @Override
  public ItemStack getIcon() {
    return itemStack;
  }

  @Override
  public IShop getShop() {
    return shop;
  }
}
