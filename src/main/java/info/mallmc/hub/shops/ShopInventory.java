package info.mallmc.hub.shops;

import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.framework.api.MallInventory;
import info.mallmc.framework.entities.VillagerNPC;
import info.mallmc.framework.util.InventoryItems;
import info.mallmc.framework.util.Messaging;
import info.mallmc.framework.util.items.ItemUtil;
import info.mallmc.hub.api.shop.IProduct;
import info.mallmc.hub.api.shop.IShop;
import java.util.List;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ShopInventory extends MallInventory {

  private MallPlayer mallPlayer;

  private String name;
  private Player player;
  private IShop shop;
  private int pageNumber = 0;
  private int previousAmount = 0;
  private int usedAmount  = 0;
  private boolean allowMultiplePlayers;
  private VillagerNPC villagerNPC;


  public ShopInventory(MallPlayer mallPlayer, String name, IShop shop, boolean allowMultiplePlayers, Player player) {
    super(mallPlayer, name, InventoryType.CHEST);
    this.mallPlayer = mallPlayer;
    this.name = name;
    this.shop = shop;
    this.allowMultiplePlayers = allowMultiplePlayers;
    this.player = player;
  }

  public ShopInventory(MallPlayer mallPlayer, String name, IShop shop, int pageNumber, int previousAmount, boolean allowMultiplePlayers, VillagerNPC villagerNPC, Player player) {
    super(mallPlayer, name, InventoryType.CHEST);
    this.mallPlayer = mallPlayer;
    this.name = name;
    this.shop = shop;
    this.pageNumber = pageNumber;
    this.previousAmount = previousAmount;
    this.allowMultiplePlayers = allowMultiplePlayers;
    this.villagerNPC = villagerNPC;
    this.player = player;
  }

  @Override
  protected void populate() {
    int amountOfRows;
    int productSize = shop.getProducts().size();
    if(pageNumber > 0){
      productSize -= previousAmount;
    }
    if(productSize % 9 == 0){
      amountOfRows = productSize / 9;
    }else{
      amountOfRows = productSize / 9 + 1;
    }
    boolean nextPageNeeded = false;
    if(amountOfRows > 6){
      nextPageNeeded = true;
      amountOfRows = 6;
    }
    usedAmount = productSize;
    if(nextPageNeeded){
      this.inventory.setItem((amountOfRows * 9 )- 2, ItemUtil.createItem(Material.ARROW, "&cNext Page"));
    }
    this.inventory.setItem((amountOfRows * 9 )- 1,InventoryItems.getCloseInventoryItem(mallPlayer));
    for(int i = 0; i < shop.getProducts().size(); i++){
      IProduct product = shop.getProducts().get(i);
      if(i >= 52){
        break;
      }
      ItemStack itemStack = product.getIcon();
      ItemMeta itemMeta  = itemStack.getItemMeta();
      itemMeta.setDisplayName(product.getDisplayName(mallPlayer));
      List<String> newLore = product.getLore(mallPlayer);
      itemMeta.setLore(newLore);
      itemStack.setItemMeta(itemMeta);
      this.inventory.setItem(i, itemStack);
    }
  }

  public ShopInventory setVillagerNPC(VillagerNPC villagerNPC) {
    this.villagerNPC = villagerNPC;
    return this;
  }

  @Override
  public void onPlayerClick(InventoryClickEvent event) {
    int i = event.getRawSlot();
    ItemStack clickedItem = player.getOpenInventory().getItem(i);
    if(clickedItem == null){
      return;
    }
    if(clickedItem.getType() == Material.ARROW){
      ShopInventory nextPage = new ShopInventory(MallPlayer.getPlayer(event.getWhoClicked().getUniqueId()), name, shop, pageNumber++, usedAmount, allowMultiplePlayers, villagerNPC, (Player)event.getWhoClicked());
      nextPage.open(player);
      return;
    }
    if(clickedItem.getType() == Material.BARRIER){
      player.getOpenInventory().close();
      return;
    }
    if(villagerNPC.isBusy()){
      Messaging.sendRawMessage(player, "global.busy", villagerNPC.getNormalName());
      return;
    }
    MallPlayer mallPlayer = MallPlayer.getPlayer(player.getUniqueId());
    if(shop.getProducts().size() < i){
      return;
    }
    IProduct iProduct = shop.getProducts().get(i);
    if(!mallPlayer.purchase(iProduct.getCost())){
      Messaging.sendMessage(mallPlayer, "global.currency.notEnough");
      return;
    }
    shop.purchaseAction(player, iProduct);
    player.getOpenInventory().close();
  }
}
