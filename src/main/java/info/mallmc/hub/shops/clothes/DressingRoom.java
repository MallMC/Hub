package info.mallmc.hub.shops.clothes;

import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.hub.api.HubPlayer;
import info.mallmc.hub.api.shop.IShop;
import info.mallmc.hub.shops.Shops;
import java.util.Collection;
import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Slime;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class DressingRoom {

  private int id;
  private Location identifierBlockLocation;
  private Location armourStandLocation;
  private Location doorLocation;
  private Location signLocation;
  private boolean occupied;
  private MallPlayer currentPlayer;

  private int currentHelm = -1;
  private int currentChest= -1;
  private int currentLegs= -1;
  private int currentBoots= -1;

  public DressingRoom(int id, Location identifierBlockLocation,
      Location armourStandLocation, Location doorLocation, Location signLocation) {
    this.id = id;
    this.identifierBlockLocation = identifierBlockLocation;
    this.armourStandLocation = armourStandLocation;
    this.doorLocation = doorLocation;
    this.signLocation = signLocation;
  }

  public boolean isOccupied() {
    return occupied;
  }

  public void setOccupied(boolean occupied) {
    this.occupied = occupied;
  }

  public MallPlayer getCurrentPlayer() {
    return currentPlayer;
  }

  public void setCurrentPlayer(MallPlayer currentPlayer) {
    this.currentPlayer = currentPlayer;
  }

  public int getId() {
    return id;
  }

  public Location getIdentifierBlockLocation() {
    return identifierBlockLocation;
  }

  public Location getArmourStandLocation() {
    return armourStandLocation;
  }

  public Location getDoorLocation() {
    return doorLocation;
  }

  public Location getSignLocation() {
    return signLocation;
  }

  public int getCurrentHelm() {
    return currentHelm;
  }

  public void setCurrentHelm(int currentHelm) {
    this.currentHelm = currentHelm;
  }

  public int getCurrentChest() {
    return currentChest;
  }

  public void setCurrentChest(int currentChest) {
    this.currentChest = currentChest;
  }

  public int getCurrentLegs() {
    return currentLegs;
  }

  public void setCurrentLegs(int currentLegs) {
    this.currentLegs = currentLegs;
  }

  public int getCurrentBoots() {
    return currentBoots;
  }

  public void setCurrentBoots(int currentBoots) {
    this.currentBoots = currentBoots;
  }

  public void enterRoom(MallPlayer mallPlayer){
    Location location = getArmourStandLocation().clone().add(0.5, 0, -0.5);
    location.setPitch(0);
    location.setYaw(180);
    ArmorStand armorStand = (ArmorStand) getIdentifierBlockLocation().getWorld().spawnEntity(location,
        EntityType.ARMOR_STAND);
    HubPlayer hubPlayer = HubPlayer.getPlayer(mallPlayer.getUuid());
    armorStand.setCustomNameVisible(false);
    armorStand.setCustomName(mallPlayer.getUsername());
    armorStand.setInvulnerable(true);
    if(hubPlayer.getEquippedClothes() != null && hubPlayer.getEquippedClothes().size() > 0){
      if(hubPlayer.getEquippedClothes().get(ClothingType.HELMET) != null) {
        armorStand.setHelmet(hubPlayer.getEquippedClothes().get(ClothingType.HELMET).getIcon());
        currentHelm = hubPlayer.getEquippedClothes().get(ClothingType.HELMET).getId();
      }
      if(hubPlayer.getEquippedClothes().get(ClothingType.CHESTPLATE) != null) {
        armorStand
            .setChestplate(hubPlayer.getEquippedClothes().get(ClothingType.CHESTPLATE).getIcon());
        currentChest = hubPlayer.getEquippedClothes().get(ClothingType.CHESTPLATE).getId();
      }
      if(hubPlayer.getEquippedClothes().get(ClothingType.LEGGINGS) != null) {
        armorStand.setLeggings(hubPlayer.getEquippedClothes().get(ClothingType.LEGGINGS).getIcon());
        currentLegs = hubPlayer.getEquippedClothes().get(ClothingType.LEGGINGS).getId();
      }
      if(hubPlayer.getEquippedClothes().get(ClothingType.BOOTS) != null) {
        armorStand.setBoots(hubPlayer.getEquippedClothes().get(ClothingType.BOOTS).getIcon());
        currentBoots = hubPlayer.getEquippedClothes().get(ClothingType.BOOTS).getId();

      }
    }
    for(int i = 0; i < 4; i++){
      ArmorStand leftArrow = (ArmorStand) getIdentifierBlockLocation().getWorld().spawnEntity(armorStand.getLocation().clone().add(1, -2 + 0.5* i , 0),
          EntityType.ARMOR_STAND);
      leftArrow.setVisible(false);
      leftArrow.setCustomNameVisible(true);
      leftArrow.setCustomName(ChatColor.BOLD + "" + ChatColor.GOLD + "<");
      leftArrow.setGravity(false);
      leftArrow.setInvulnerable(true);
      Slime leftArrowSlime = (Slime) getArmourStandLocation().getWorld().spawnEntity(leftArrow.getLocation().clone().add(0, 2.25, 0), EntityType.SLIME);
      leftArrowSlime.setSize(1);
      leftArrowSlime.setSilent(true);
      leftArrowSlime.setInvulnerable(true);
      leftArrowSlime.setCustomName(id + "_left_" + i);
      leftArrowSlime.setCustomNameVisible(false);
      leftArrowSlime.setAI(false);
      leftArrow.getPassengers().add(leftArrowSlime);
      leftArrowSlime.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 0, true, false));
      ArmorStand rightArrow = (ArmorStand) getIdentifierBlockLocation().getWorld().spawnEntity(armorStand.getLocation().clone().add(-1, -2 + 0.5* i , 0),
          EntityType.ARMOR_STAND);
      rightArrow.setVisible(false);
      rightArrow.setCustomNameVisible(true);
      rightArrow.setCustomName(ChatColor.BOLD + "" + ChatColor.GOLD + ">");
      rightArrow.setGravity(false);
      rightArrow.setInvulnerable(true);
      Slime rightArrowSlime = (Slime) getArmourStandLocation().getWorld().spawnEntity(rightArrow.getLocation().clone().add(0, 2.25, 0), EntityType.SLIME);
      rightArrowSlime.setSize(1);
      rightArrowSlime.setSilent(true);
      rightArrowSlime.setInvulnerable(true);
      rightArrowSlime.setCustomName(id + "_right_" + i);
      rightArrowSlime.setCustomNameVisible(false);
      rightArrowSlime.setAI(false);
      rightArrow.getPassengers().add(rightArrowSlime);
      rightArrowSlime.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 0, true, false));
    }
  }
  public void onLeave(MallPlayer mallPlayer){
    if(mallPlayer == null){
      Collection<Entity> entities = this.getArmourStandLocation().getWorld().getNearbyEntities(this.getArmourStandLocation(), 1.5 , 1.5, 1.5);
      for(Entity entity : entities){
        if(entity instanceof ArmorStand || entity instanceof Slime){
          entity.remove();
        }
      }
      this.setCurrentBoots(-1);
      this.setCurrentLegs(-1);
      this.setCurrentChest(-1);
      this.setCurrentHelm(-1);
      this.setCurrentPlayer(null);
      this.setOccupied(false);
      return;
    }
    HubPlayer hubPlayer = HubPlayer.getPlayer(mallPlayer.getUuid());
    Player player = Bukkit.getPlayer(mallPlayer.getUuid());
    IShop shop = Shops.getRegisteredShops().get("Clothes");
    ClothesShop clothesShop = (ClothesShop) shop;
    HashMap<ClothingType, ClothesProduct> equipped = hubPlayer.getEquippedClothes();
    if(equipped == null){
      equipped = new HashMap<>();
    }
    equipped.clear();
    ClothesProduct helmet = clothesShop.getClothesProducts().get(ClothingType.HELMET).get(this.getCurrentHelm());
    if(helmet != null && hubPlayer.getUnlockedClotheProducts().get(ClothingType.HELMET).contains(helmet)){
      equipped.put(ClothingType.HELMET, helmet);
      player.getInventory().setHelmet(helmet.getIcon());
    }else{
      if(currentHelm == -1){
        player.getInventory().setHelmet(null);
      }
      if(hubPlayer.getEquippedClothes() != null && hubPlayer.getEquippedClothes().get(ClothingType.HELMET) != null){
        equipped.put(ClothingType.HELMET, hubPlayer.getEquippedClothes().get(ClothingType.HELMET));
      }
    }
    ClothesProduct chest = clothesShop.getClothesProducts().get(ClothingType.CHESTPLATE).get(this.getCurrentChest());
    if(chest != null && hubPlayer.getUnlockedClotheProducts().get(ClothingType.CHESTPLATE).contains(chest)){
      equipped.put(ClothingType.CHESTPLATE, chest);
      player.getInventory().setChestplate(chest.getIcon());
    }else{
      if(currentChest == -1){
        player.getInventory().setChestplate(null);
      }
      if(hubPlayer.getEquippedClothes() != null && hubPlayer.getEquippedClothes().get(ClothingType.CHESTPLATE) != null){
        equipped.put(ClothingType.CHESTPLATE, hubPlayer.getEquippedClothes().get(ClothingType.CHESTPLATE));
      }
    }
    ClothesProduct legs = clothesShop.getClothesProducts().get(ClothingType.LEGGINGS).get(this.getCurrentLegs());
    if(legs != null &&hubPlayer.getUnlockedClotheProducts().get(ClothingType.LEGGINGS).contains(legs)){
      equipped.put(ClothingType.LEGGINGS, legs);
      player.getInventory().setLeggings(legs.getIcon());
    }else{
      if(currentLegs == -1){
        player.getInventory().setLeggings(null);
      }
      if(hubPlayer.getEquippedClothes() != null && hubPlayer.getEquippedClothes().get(ClothingType.LEGGINGS) != null){
        equipped.put(ClothingType.LEGGINGS, hubPlayer.getEquippedClothes().get(ClothingType.LEGGINGS)); }
    }
    ClothesProduct boots = clothesShop.getClothesProducts().get(ClothingType.BOOTS).get(this.getCurrentBoots());
    if(boots != null &&hubPlayer.getUnlockedClotheProducts().get(ClothingType.BOOTS).contains(boots)){
      equipped.put(ClothingType.BOOTS, boots);
      player.getInventory().setBoots(boots.getIcon());
    }else{
      if(currentBoots == -1){
        player.getInventory().setBoots(null);
      }
      if(hubPlayer.getEquippedClothes() != null && hubPlayer.getEquippedClothes().get(ClothingType.BOOTS) != null){
        equipped.put(ClothingType.BOOTS, hubPlayer.getEquippedClothes().get(ClothingType.BOOTS));
      }
    }
    hubPlayer.setEquippedClothes(equipped);
    Collection<Entity> entities = this.getArmourStandLocation().getWorld().getNearbyEntities(this.getArmourStandLocation(), 1.5 , 1.5, 1.5);
    for(Entity entity : entities){
      if(entity instanceof ArmorStand || entity instanceof Slime){
        entity.remove();
      }
    }
    this.setCurrentBoots(-1);
    this.setCurrentLegs(-1);
    this.setCurrentChest(-1);
    this.setCurrentHelm(-1);
    this.setCurrentPlayer(null);
    this.setOccupied(false);
  }
}
