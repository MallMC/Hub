package info.mallmc.hub.shops.clothes;

import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.framework.interactions.Interaction;
import info.mallmc.framework.interactions.InteractionRegistry;
import info.mallmc.framework.util.Messaging;
import info.mallmc.framework.util.items.ItemUtil;
import info.mallmc.hub.Hub;
import info.mallmc.hub.api.HubPlayer;
import info.mallmc.hub.api.shop.EnumShopState;
import info.mallmc.hub.api.shop.IProduct;
import info.mallmc.hub.api.shop.IShop;
import info.mallmc.hub.api.shop.ShopArea;
import info.mallmc.hub.api.shop.ShopDoor;
import info.mallmc.hub.api.shop.ShopDoor.DoorType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Slime;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

public class ClothesShop implements IShop{

  private EnumShopState shopState = null;
  private HashMap<Vector, DressingRoom> dressingRooms;
  private HashMap<ClothingType, HashMap<Integer, ClothesProduct>> clothesProducts;

  @Override
  public ShopArea getShopArea() {
    return new ShopArea(new Location(Hub.getInstance().getHubWorld(), 66, 69, -26), new Location(Hub.getInstance().getHubWorld(), 84, 74, -1));
  }

  @Override
  public String getName() {
    return "Clothes";
  }

  @Override
  public boolean hasMusic() {
    return false;
  }

  @Override
  public EnumShopState getShopState() {
    return shopState;
  }

  @Override
  public boolean setShopState(EnumShopState enumShopState) {
    if (this.shopState == enumShopState) {
      return false;
    } else {
      this.shopState = enumShopState;
      if(shopState.isOpen()) {
        getDoor().openDoor();
        return true;
      }else {
        getDoor().closeDoor();
        return true;
      }
    }
  }

  @Override
  public ShopDoor getDoor() {
    return new ShopDoor(new Location(Hub.getInstance().getHubWorld(), 76, 73, -27), new Location(Hub.getInstance().getHubWorld(), 73, 71, -27), (byte) 6, DoorType.UP_AND_DOWN);
  }

  @Override
  public void registerInventories() {

  }

  @Override
  public void registerInteractions() {
    //TODO Armour Stand Malark
    InteractionRegistry.registerInteraction(Interaction.MOVE, (event) -> {
       PlayerMoveEvent playerMoveEvent = (PlayerMoveEvent) event;
        if(getShopArea().isInArea(playerMoveEvent.getTo())){
          if(playerMoveEvent.getTo().getBlock().getType() == Material.WOOD_PLATE){
            Location identifier = playerMoveEvent.getTo().clone().add(0, -1, 0);
            if(identifier.getBlock().getType() == Material.GOLD_BLOCK){
              DressingRoom dressingRoom = dressingRooms.get(identifier.getBlock().getLocation().toVector());
              if(dressingRoom == null){
                return;
              }
              MallPlayer mallPlayer = MallPlayer.getPlayer(playerMoveEvent.getPlayer().getUniqueId());
              dressingRoom.setOccupied(true);
              dressingRoom.setCurrentPlayer(mallPlayer);
              identifier.getBlock().setType(Material.EMERALD_BLOCK);
              final Block block =
                  identifier.clone().add(0, 1, 0).getBlock();
              block.setType(Material.AIR);
              Bukkit.getScheduler().scheduleSyncDelayedTask(Hub.getInstance(), () -> block.setType(Material.WOOD_PLATE), 20L);
              dressingRoom.enterRoom(mallPlayer);
            }else if(identifier.getBlock().getType() == Material.EMERALD_BLOCK){
              DressingRoom dressingRoom = dressingRooms.get(identifier.getBlock().getLocation().toVector());
              if(dressingRoom == null){
                return;
              }
              MallPlayer mallPlayer = MallPlayer.getPlayer(playerMoveEvent.getPlayer().getUniqueId());
              dressingRoom.onLeave(mallPlayer);
              identifier.getBlock().setType(Material.GOLD_BLOCK);
              final Block block =
                  identifier.clone().add(0, 1, 0).getBlock();
              block.setType(Material.AIR);
              Bukkit.getScheduler().scheduleSyncDelayedTask(Hub.getInstance(), () -> block.setType(Material.WOOD_PLATE), 20L);
            }
          }
        }
    });
    InteractionRegistry.registerInteraction(Interaction.RIGHT_CLICK_BLOCK, (event) -> {
      PlayerInteractEvent playerInteractEvent = (PlayerInteractEvent) event;
      if(playerInteractEvent.getClickedBlock().getType() == Material.WOODEN_DOOR || playerInteractEvent.getClickedBlock().getType() == Material.WOOD_DOOR){
        if(getShopArea().isInArea(playerInteractEvent.getPlayer())){
          for(DressingRoom dressingRoom : dressingRooms.values()){
            if(dressingRoom.getDoorLocation().equals(playerInteractEvent.getClickedBlock().getLocation()) || dressingRoom.getDoorLocation().equals(playerInteractEvent.getClickedBlock().getLocation().clone().add(0, 1, 0))){
              if(dressingRoom.isOccupied()){
                playerInteractEvent.setCancelled(true);
                break;
              }
            }
          }
        }
      }
    });
    InteractionRegistry.registerInteraction(Interaction.RIGHT_CLICK_ENTITY, (event) -> {
      PlayerInteractEntityEvent playerInteractEvent = (PlayerInteractEntityEvent) event;
      if(playerInteractEvent.getRightClicked() instanceof Slime){
        if(playerInteractEvent.getHand() != EquipmentSlot.HAND){
          return;
        }
        Slime slime = (Slime) playerInteractEvent.getRightClicked();
        String name = slime.getCustomName();
        String[] parts = name.split("_");
        String side = parts[1];
        int dressingRoomID = Integer.parseInt(parts[0]);
        int partID = Integer.parseInt(parts[2]);
        DressingRoom dressingRoom = null;
        for(DressingRoom dressingRom : dressingRooms.values()){
          if(dressingRom.getId() == dressingRoomID){
            dressingRoom = dressingRom;
          }
        }
        if(dressingRoom == null){
          return;
        }
        if(!playerInteractEvent.getPlayer().getUniqueId().toString().equals(dressingRoom.getCurrentPlayer().getUuid().toString())){
          playerInteractEvent.setCancelled(true);
          return;
        }
        ArmorStand armorStand = null;
        Collection<Entity> entities = dressingRoom.getArmourStandLocation().getWorld().getNearbyEntities(dressingRoom.getArmourStandLocation(), 1 , 1, 1);
        for(Entity entity : entities){
          if(entity instanceof ArmorStand){
            if(entity.getCustomName().equalsIgnoreCase(playerInteractEvent.getPlayer().getName())){
              armorStand = (ArmorStand) entity;
              break;
            }
          }
        }
        if(side.equalsIgnoreCase("buy")) {
          HubPlayer hubPlayer = HubPlayer.getPlayer(dressingRoom.getCurrentPlayer().getUuid());
          ClothesProduct helmet = clothesProducts.get(ClothingType.HELMET).get(dressingRoom.getCurrentHelm());
          if(helmet != null && !hubPlayer.getUnlockedClotheProducts().get(ClothingType.HELMET).contains(helmet)){
            if(dressingRoom.getCurrentPlayer().purchase(helmet.getCost())){
              hubPlayer.getUnlockedClotheProducts().get(ClothingType.HELMET).add(helmet);
            }else{
              Messaging.sendMessage(dressingRoom.getCurrentPlayer(), "global.currency.notEnough");
              return;
            }
          }
          ClothesProduct chest = clothesProducts.get(ClothingType.CHESTPLATE).get(dressingRoom.getCurrentChest());
          if(chest != null && !hubPlayer.getUnlockedClotheProducts().get(ClothingType.CHESTPLATE).contains(chest)){
            if(dressingRoom.getCurrentPlayer().purchase(chest.getCost())){
              hubPlayer.getUnlockedClotheProducts().get(ClothingType.CHESTPLATE).add(chest);
            }else{
              Messaging.sendMessage(dressingRoom.getCurrentPlayer(), "global.currency.notEnough");
              return;
            }
          }
          ClothesProduct legs = clothesProducts.get(ClothingType.LEGGINGS).get(dressingRoom.getCurrentLegs());
          if(legs != null && !hubPlayer.getUnlockedClotheProducts().get(ClothingType.LEGGINGS).contains(legs)){
            if(dressingRoom.getCurrentPlayer().purchase(legs.getCost())){
              hubPlayer.getUnlockedClotheProducts().get(ClothingType.LEGGINGS).add(legs);
            }else{
              Messaging.sendMessage(dressingRoom.getCurrentPlayer(), "global.currency.notEnough");
              return;
            }
          }
          ClothesProduct boots = clothesProducts.get(ClothingType.BOOTS).get(dressingRoom.getCurrentBoots());
          if(boots != null && !hubPlayer.getUnlockedClotheProducts().get(ClothingType.BOOTS).contains(boots)){
            if(dressingRoom.getCurrentPlayer().purchase(boots.getCost())){
              hubPlayer.getUnlockedClotheProducts().get(ClothingType.BOOTS).add(boots);
            }else{
              Messaging.sendMessage(dressingRoom.getCurrentPlayer(), "global.currency.notEnough");
              return;
            }
          }for(Entity entity : entities){
            if(entity instanceof ArmorStand){
              if(entity.getCustomName().equalsIgnoreCase("Buy Clothing items")){
                entity.remove();
                break;
              }
            }
          }
          return;
        }
        switch(partID){
          case 0:
            //LEGGINGS
            int currentBoots = dressingRoom.getCurrentBoots();
            int nextBoots = getNextItem(currentBoots, side, ClothingType.BOOTS);
            if(nextBoots == -1){
              armorStand.setBoots(null);
              dressingRoom.setCurrentBoots(nextBoots);
              return;
            }
            ClothesProduct bootsProduct = clothesProducts.get(ClothingType.BOOTS).get(nextBoots);
            ItemStack bootsProductIcon = bootsProduct.getIcon();
            armorStand.setBoots(bootsProductIcon);
            dressingRoom.setCurrentBoots(nextBoots);
            HubPlayer bootsHubPlayer = HubPlayer.getPlayer(dressingRoom.getCurrentPlayer().getUuid());
            showBuy(dressingRoom, bootsHubPlayer, bootsProduct, ClothingType.BOOTS);
            break;
          case 1:
            //LEGGINGS
            int currentLegs = dressingRoom.getCurrentLegs();
            int nextLegs = getNextItem(currentLegs, side, ClothingType.LEGGINGS);
            if(nextLegs == -1){
              armorStand.setLeggings(null);
              dressingRoom.setCurrentLegs(nextLegs);
              return;
            }
            ClothesProduct legsProduct = clothesProducts.get(ClothingType.LEGGINGS).get(nextLegs);
            ItemStack legsProductIcon = legsProduct.getIcon();
            armorStand.setLeggings(legsProductIcon);
            dressingRoom.setCurrentLegs(nextLegs);
            HubPlayer legsHubPlayer = HubPlayer.getPlayer(dressingRoom.getCurrentPlayer().getUuid());
            showBuy(dressingRoom, legsHubPlayer, legsProduct, ClothingType.LEGGINGS);
            break;
          case 2:
            //CHEST
            int currentChest = dressingRoom.getCurrentChest();
            int nextChest = getNextItem(currentChest, side, ClothingType.CHESTPLATE);
            if(nextChest == -1){
              armorStand.setChestplate(null);
              dressingRoom.setCurrentChest(nextChest);
              return;
            }
            ClothesProduct chestProduct = clothesProducts.get(ClothingType.CHESTPLATE).get(nextChest);
            ItemStack chestProductIcon = chestProduct.getIcon();
            armorStand.setChestplate(chestProductIcon);
            dressingRoom.setCurrentChest(nextChest);
            HubPlayer chestHubplayer = HubPlayer.getPlayer(dressingRoom.getCurrentPlayer().getUuid());
            showBuy(dressingRoom, chestHubplayer, chestProduct, ClothingType.CHESTPLATE);
            break;
          case 3:
            //HELMET
            int currentHelmet = dressingRoom.getCurrentHelm();
            int nextHelmet = getNextItem(currentHelmet, side, ClothingType.HELMET);
            if(nextHelmet == -1){
              armorStand.setHelmet(null);
              dressingRoom.setCurrentHelm(nextHelmet);
              return;
            }
            ClothesProduct clothesProduct = clothesProducts.get(ClothingType.HELMET).get(nextHelmet);
            ItemStack nextHelmetItem = clothesProduct.getIcon();
            armorStand.setHelmet(nextHelmetItem);
            dressingRoom.setCurrentHelm(nextHelmet);
            HubPlayer helmetHelmPlayer = HubPlayer.getPlayer(dressingRoom.getCurrentPlayer().getUuid());
            showBuy(dressingRoom, helmetHelmPlayer, clothesProduct, ClothingType.HELMET);
            break;
        }
      }else if(playerInteractEvent.getRightClicked() instanceof ArmorStand){
        ((PlayerInteractEntityEvent) event).setCancelled(true);
      }

    });
  }

  public HashMap<ClothingType, HashMap<Integer, ClothesProduct>> getClothesProducts() {
    return clothesProducts;
  }

  @Override
  public void registerEntities() {
    //TODO Armour Stand
  }

  public DressingRoom isInRoom(Player player){
    for(DressingRoom dressingRoom : dressingRooms.values()){
      if(dressingRoom.isOccupied()){
        if(dressingRoom.getCurrentPlayer().getUuid().equals(player.getUniqueId())){
          return dressingRoom;
        }
      }
    }
    return null;
  }

  private int getNextItem(int currentItem, String side, ClothingType type){
    int nextHelmet = side.equals("left") ? currentItem + 1 : currentItem - 1;
    if(nextHelmet < -1){
      nextHelmet = clothesProducts.get(type).size() - 1;
    }
    if(nextHelmet >= clothesProducts.get(type).size()){
      nextHelmet = -1;
    }
    return nextHelmet;
  }

  private void showBuy(DressingRoom dressingRoom, HubPlayer hubPlayer, ClothesProduct clothesProduct, ClothingType clothingType){
    if(hubPlayer.getUnlockedClotheProducts() == null){
      hubPlayer.setUnlockedClotheProducts(new HashMap<>());
    }
    if(hubPlayer.getUnlockedClotheProducts().get(clothingType) == null){
      hubPlayer.getUnlockedClotheProducts().put(clothingType, new ArrayList<>());
    }
    if(!hubPlayer.getUnlockedClotheProducts().get(clothingType).contains(clothesProduct)){
      Collection<Entity> entities = dressingRoom.getArmourStandLocation().getWorld().getNearbyEntities(dressingRoom.getArmourStandLocation(), 1.5 , 1.5, 1.5);
      boolean exists = false;
      for(Entity entity : entities){
        if(entity instanceof ArmorStand){
          if(entity.getCustomName().equalsIgnoreCase("Buy Clothing items")){
            exists = true;
            break;
          }
        }
      }
      if(!exists){
        ArmorStand leftArrow = (ArmorStand) dressingRoom.getArmourStandLocation().getWorld().spawnEntity(dressingRoom.getArmourStandLocation().clone().add(0.5, 0 , -0.5),
            EntityType.ARMOR_STAND);
        leftArrow.setVisible(false);
        leftArrow.setCustomNameVisible(true);
        leftArrow.setCustomName("Buy Clothing items");
        leftArrow.setGravity(false);
        leftArrow.setInvulnerable(true);
        Slime leftArrowSlime = (Slime) dressingRoom.getArmourStandLocation().getWorld().spawnEntity(leftArrow.getLocation().clone().add(0, 2, 0), EntityType.SLIME);
        leftArrowSlime.setSize(1);
        leftArrowSlime.setSilent(true);
        leftArrowSlime.setInvulnerable(true);
        leftArrowSlime.setCustomName(dressingRoom.getId() + "_buy_" + 1);
        leftArrowSlime.setCustomNameVisible(false);
        leftArrowSlime.setAI(false);
        leftArrow.getPassengers().add(leftArrowSlime);
        leftArrowSlime.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 0, true, false));
      }
    }
  }

  @Override
  public void onPlayerEnter(Player player) {

  }

  @Override
  public void onPlayerLeave(Player player) {

  }

  @Override
  public void onRegister() {
    setShopState(EnumShopState.OPEN);
    registerInventories();
    registerEntities();
    registerInteractions();
    dressingRooms = new HashMap<>();
    dressingRooms.put(new Location(Hub.getInstance().getHubWorld(), 68, 70, -24, 180, 0).toVector(), new DressingRoom(1, new Location(Hub.getInstance().getHubWorld(), 68, 70, -24), new Location(Hub.getInstance().getHubWorld(), 67, 71, -21), new Location(Hub.getInstance().getHubWorld(), 69, 71, -24), new Location(Hub.getInstance().getHubWorld(), 70, 73, -24)));
    clothesProducts = new HashMap<>();
    clothesProducts.put(ClothingType.HELMET, new HashMap<>());
    clothesProducts.put(ClothingType.CHESTPLATE, new HashMap<>());
    clothesProducts.put(ClothingType.LEGGINGS, new HashMap<>());
    clothesProducts.put(ClothingType.BOOTS, new HashMap<>());
    clothesProducts.get(ClothingType.HELMET).put(0, new ClothesProduct("leather_helm", 1,
        ItemUtil.createStack(Material.LEATHER_HELMET, 1), this, 0));
    clothesProducts.get(ClothingType.HELMET).put(1, new ClothesProduct("iron_helm", 2,
        ItemUtil.createStack(Material.IRON_HELMET, 1), this, 1));
    clothesProducts.get(ClothingType.CHESTPLATE).put(0, new ClothesProduct("leather_chest", 1,
        ItemUtil.createStack(Material.LEATHER_CHESTPLATE, 1), this, 0));
    clothesProducts.get(ClothingType.CHESTPLATE).put(1, new ClothesProduct("iron_chest", 2,
        ItemUtil.createStack(Material.IRON_CHESTPLATE, 1), this, 1));
    clothesProducts.get(ClothingType.LEGGINGS).put(0, new ClothesProduct("leather_legs", 1,
        ItemUtil.createStack(Material.LEATHER_LEGGINGS, 1), this, 0));
    clothesProducts.get(ClothingType.LEGGINGS).put(1, new ClothesProduct("iron_legs", 2,
        ItemUtil.createStack(Material.IRON_LEGGINGS, 1), this, 1));
    clothesProducts.get(ClothingType.BOOTS).put(0, new ClothesProduct("leather_boots", 1,
        ItemUtil.createStack(Material.LEATHER_BOOTS, 1), this, 0));
    clothesProducts.get(ClothingType.BOOTS).put(1, new ClothesProduct("iron_boots", 2,
        ItemUtil.createStack(Material.IRON_BOOTS, 1), this, 1));
  }

  @Override
  public void onEnable() {
  }

  @Override
  public void onDisable() {

  }

  @Override
  public void purchaseAction(Player player, IProduct product) {

  }

  @Override
  public List<IProduct> getProducts() {
    return null;
  }
}
