package info.mallmc.hub.shops.clothes;

public enum ClothingType {

  HELMET,
  CHESTPLATE,
  LEGGINGS,
  BOOTS;
}
