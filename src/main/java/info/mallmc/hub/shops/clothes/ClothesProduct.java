package info.mallmc.hub.shops.clothes;

import info.mallmc.hub.api.shop.IProduct;
import info.mallmc.hub.api.shop.IShop;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class ClothesProduct implements IProduct{

  private String name;
  private int cost;
  private ItemStack itemStack;
  private IShop shop;
  private int id;

  public ClothesProduct(String name, int cost, ItemStack itemStack, IShop shop, int id) {
    this.name = name;
    this.cost = cost;
    this.itemStack = itemStack;
    this.shop = shop;
    this.id =id;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public int getCost() {
    return cost;
  }

  @Override
  public void onInteract(PlayerInteractEvent playerInteractEvent) {

  }

  @Override
  public ItemStack getIcon() {
    return itemStack;
  }

  @Override
  public IShop getShop() {
    return shop;
  }

  public int getId() {
    return id;
  }
}
