package info.mallmc.hub.shops.airship;

import info.mallmc.hub.Hub;
import info.mallmc.hub.api.shop.*;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.List;

public class AirShipShop implements IShop {

    private EnumShopState shopState = null;

    @Override
    public ShopArea getShopArea() {
        return new ShopArea(new Location(Hub.getInstance().getHubWorld(), 64, 74, -62), new Location(Hub.getInstance().getHubWorld(), 49, 71, -77));
    }

    @Override
    public String getName() {
        return "AirShip";
    }

    @Override
    public boolean hasMusic() {
        return false;
    }

    @Override
    public EnumShopState getShopState() {
        return shopState;
    }

    @Override
    public boolean setShopState(EnumShopState enumShopState) {
        if (this.shopState == enumShopState) {
            return false;
        } else {
            this.shopState = enumShopState;
            if(shopState.isOpen()) {
                getDoor().openDoor();
                return true;
            }else {
                getDoor().closeDoor();
                return true;
            }
        }
    }

    @Override
    public ShopDoor getDoor() {
        return new ShopDoor(new Location(Hub.getInstance().getHubWorld(), 55, 73, -61), new Location(Hub.getInstance().getHubWorld(), 58, 71, -61), (byte) 4, ShopDoor.DoorType.UP_AND_DOWN);
    }

    @Override
    public void registerInventories() {

    }

    @Override
    public void registerInteractions() {

    }

    @Override
    public void registerEntities() {

    }

    @Override
    public void onPlayerEnter(Player player) {

    }

    @Override
    public void onPlayerLeave(Player player) {

    }

    @Override
    public void onEnable() {

    }

    @Override
    public void onDisable() {

    }

    @Override
    public void onRegister() {
        setShopState(EnumShopState.OPEN);
    }

    @Override
    public void purchaseAction(Player player, IProduct product) {

    }

    @Override
    public List<IProduct> getProducts() {
        return null;
    }

}
