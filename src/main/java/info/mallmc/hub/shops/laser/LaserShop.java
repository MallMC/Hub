package info.mallmc.hub.shops.laser;

import info.mallmc.hub.Hub;
import info.mallmc.hub.api.shop.*;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.List;

public class LaserShop implements IShop {


    private EnumShopState shopState = null;

    @Override
    public ShopArea getShopArea() {
        return new ShopArea(new Location(Hub.getInstance().getHubWorld(), 67, 64, -26), new Location(Hub.getInstance().getHubWorld(), 82, 61, -11));
    }

    @Override
    public String getName() {
        return "Laser";
    }

    @Override
    public boolean hasMusic() {
        return false;
    }

    @Override
    public EnumShopState getShopState() {
        return shopState;
    }

    @Override
    public boolean setShopState(EnumShopState enumShopState) {
        if (this.shopState == enumShopState) {
            return false;
        } else {
            this.shopState = enumShopState;
            if(shopState.isOpen()) {
                getDoor().openDoor();
                return true;
            }else {
                getDoor().closeDoor();
                return true;
            }
        }
    }

    @Override
    public ShopDoor getDoor() {
            return new ShopDoor(new Location(Hub.getInstance().getHubWorld(), 73, 63, -27), new Location(Hub.getInstance().getHubWorld(), 76, 61, -27), (byte) 5, ShopDoor.DoorType.UP_AND_DOWN);
    }

    @Override
    public void registerInventories() {

    }

    @Override
    public void registerInteractions() {

    }
    @Override
    public void purchaseAction(Player player, IProduct product) {

    }

    @Override
    public void registerEntities() {

    }

    @Override
    public void onPlayerEnter(Player player) {

    }

    @Override
    public void onPlayerLeave(Player player) {

    }

    @Override
    public void onEnable() {

    }

    @Override
    public void onDisable() {

    }

    @Override
    public void onRegister() {
        setShopState(EnumShopState.OPEN);
    }

    @Override
    public List<IProduct> getProducts() {
        return null;
    }

}
