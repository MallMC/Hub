package info.mallmc.hub.shops;

import info.mallmc.framework.util.jukebox.MallJukebox;
import info.mallmc.framework.util.jukebox.MallMedia;
import info.mallmc.hub.api.shop.IShop;

import javax.annotation.Nullable;
import java.util.HashMap;

public class Shops {

  private static HashMap<String, MallJukebox> shopsJukeboxs = new HashMap<>();

  private static HashMap<String, IShop> registeredShops = new HashMap<>();

  public static HashMap<String, IShop> getRegisteredShops(){return registeredShops;}

  public static void registerShop(IShop shop){
    registeredShops.put(shop.getName(), shop);
    shop.onRegister();

    if(shop.hasMusic()) {
      MallJukebox shopJukebox = new MallJukebox("hub_" + shop.getName().toLowerCase());
      shopJukebox.playSoundForShow(new MallMedia(shop.getMusicURL(), true));
      shopsJukeboxs.put(shop.getName(), shopJukebox);
    }
  }

  public static IShop getShop(String name){
    return registeredShops.get(name);
  }

  @Nullable
  public static MallJukebox getShopJuekbox(IShop iShop) {
    if (shopsJukeboxs.containsKey(iShop.getName())) {
      return shopsJukeboxs.get(iShop.getName());
    }
    return null;
  }

  @Nullable
  public static MallJukebox getShopJuekbox(String name) {
    if (shopsJukeboxs.containsKey(name)) {
      return shopsJukeboxs.get(name);
    }
    return null;
  }
}
