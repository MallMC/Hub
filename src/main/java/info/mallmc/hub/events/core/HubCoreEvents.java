package info.mallmc.hub.events.core;

import info.mallmc.core.events.Event;
import info.mallmc.core.events.EventHandler;
import info.mallmc.core.events.EventType;
import info.mallmc.core.events.events.PlayerChangeEvent;
import info.mallmc.hub.Hub;

public class HubCoreEvents
{
    public HubCoreEvents()
    {
        EventHandler.registerEvent(EventType.PLAYER_CHANGE, this::onPlayerChange);
    }

    private void onPlayerChange(Event event)
    {
        PlayerChangeEvent playerChangeEvent = (PlayerChangeEvent) event;
        Hub.getInstance().getScoreboardManager().displayLobbyScorboard(playerChangeEvent.getUpdatedPlayer());
    }
}
