package info.mallmc.hub.events;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import info.mallmc.framework.interactions.Interaction;
import info.mallmc.framework.interactions.InteractionRegistry;
import info.mallmc.framework.util.ReflectionUtils;
import info.mallmc.hub.api.shop.IProduct;
import info.mallmc.hub.api.shop.IShop;
import info.mallmc.hub.inventories.NavigationInventory;
import info.mallmc.hub.inventories.PlayerInformationInventory;
import info.mallmc.hub.shops.Shops;
import info.mallmc.hub.util.HotbarItems;
import net.minecraft.server.v1_12_R1.NBTTagCompound;
import net.minecraft.server.v1_12_R1.NBTTagList;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.block.CraftSkull;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.craftbukkit.v1_12_R1.util.CraftMagicNumbers.NBT;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.material.Skull;

public class HubInteractions {

  public static void registerInteractions(){
    InteractionRegistry.registerInteraction(Interaction.RIGHT_CLICK, HubInteractions::handleInteract);
  }


  public static void handleInteract(Event event){
    PlayerInteractEvent playerInteractEvent = (PlayerInteractEvent) event;
    if(playerInteractEvent.getItem() == null){
      return;
    }
    if(playerInteractEvent.getItem().getType().equals(HotbarItems.NAVIGATION.getMaterial())){
      new NavigationInventory(playerInteractEvent.getPlayer()).open(playerInteractEvent.getPlayer());
      playerInteractEvent.setCancelled(true);
    }else if(playerInteractEvent.getItem().getType().equals(HotbarItems.PLAYER_INFO.getMaterial())){
      SkullMeta skullMeta = ((SkullMeta)playerInteractEvent.getItem().getItemMeta());
      if(skullMeta.hasOwner() && skullMeta.getOwningPlayer().getName().equalsIgnoreCase(playerInteractEvent.getPlayer().getName())){
        new PlayerInformationInventory(playerInteractEvent.getPlayer()).open(playerInteractEvent.getPlayer());
        playerInteractEvent.setCancelled(true);
      }else {
        net.minecraft.server.v1_12_R1.ItemStack nmsHead = CraftItemStack.asNMSCopy(playerInteractEvent.getItem());
        NBTTagCompound nbtTagCompound = (nmsHead.hasTag()) ? nmsHead.getTag() : new NBTTagCompound();
        NBTTagCompound nbtTagCompound1 = nbtTagCompound.getCompound("SkullOwner");
        NBTTagCompound properties  = nbtTagCompound1.getCompound("Properties");
        NBTTagList textures = properties.getList("textures", 10);
        NBTTagCompound texture = textures.get(0);
        String textureString  = texture.getString("Value");
        for(IShop iShop : Shops.getRegisteredShops().values()){
          if(iShop.getProducts() == null){
            continue;
          }
          for(IProduct product : iShop.getProducts()){
            net.minecraft.server.v1_12_R1.ItemStack nmsHead2 = CraftItemStack.asNMSCopy(product.getIcon());
            NBTTagCompound rootCompound = (nmsHead2.hasTag()) ? nmsHead2.getTag() : new NBTTagCompound();
            NBTTagCompound ownerCompound = rootCompound.getCompound("SkullOwner");
            NBTTagCompound iconProperties  = ownerCompound.getCompound("Properties");
            NBTTagList iconTextures = iconProperties.getList("textures", 10);
            NBTTagCompound iconTexture = iconTextures.get(0);
            String iconTextureString  = iconTexture.getString("Value");
            if(iconTextureString.equals(textureString)){
              product.onInteract(playerInteractEvent);
            }
          }
        }
      }
    }
  }


}
