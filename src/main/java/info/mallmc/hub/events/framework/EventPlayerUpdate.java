package info.mallmc.hub.events.framework;

import info.mallmc.core.events.EventHandler;
import info.mallmc.core.events.EventType;
import info.mallmc.core.events.events.PlayerChangeEvent;
import info.mallmc.hub.Hub;

/**
 * Created by UnRealDinnerbone on 8/25/2017.
 */
public class EventPlayerUpdate
{
    public EventPlayerUpdate()
    {
        EventHandler.registerEvent(EventType.PLAYER_CHANGE, event -> onUpdate((PlayerChangeEvent) event));
    }

    public void onUpdate(PlayerChangeEvent eventPlayerUpdate) {
        Hub.getInstance().getScoreboardManager().displayLobbyScorboard(eventPlayerUpdate.getUpdatedPlayer());
    }
}
