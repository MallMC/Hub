package info.mallmc.hub.events;

import info.mallmc.core.MallCore;
import info.mallmc.core.api.logs.LL;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.framework.util.Messaging;
import info.mallmc.framework.util.jukebox.MallJukebox;
import info.mallmc.hub.Hub;
import info.mallmc.hub.api.HubPlayer;
import info.mallmc.hub.api.HubPlayer.PlayerVisibility;
import info.mallmc.hub.api.shop.IShop;
import info.mallmc.hub.shops.Shops;
import info.mallmc.hub.shops.clothes.ClothesProduct;
import info.mallmc.hub.shops.clothes.ClothesShop;
import info.mallmc.hub.shops.clothes.ClothingType;
import info.mallmc.hub.shops.clothes.DressingRoom;
import info.mallmc.hub.util.HotbarItems;
import info.mallmc.hub.util.HubPlayerData;
import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.libs.jline.internal.Log;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class HubPlayerEvents implements Listener
{
    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event)
    {
        Player playerMoved = event.getPlayer();
        for(IShop iShop : Shops.getRegisteredShops().values()){
            if(!iShop.getShopArea().isInArea(event.getFrom())){
                if(iShop.getShopArea().isInArea(event.getTo())){
                    iShop.onPlayerEnter(playerMoved);
                    Messaging.sendMessage(event.getPlayer(), "hub.shops." + iShop.getName() + ".welcome");
                    if(iShop.hasMusic()) {
                        if (iShop.getJukebox() != null) {
                            Hub.getInstance().getHubJukebox().removePlayer(playerMoved);
                            iShop.getJukebox().addPlayer(playerMoved);
                        } else {
                            Log.error(LL.ERROR, MallCore.getInstance().getServer().getNickname(), "Player Movement -> getting Malljukebox", "MallJukebox is null", "MallJukebox is null while there was a music url given");
                        }
                    }
                }
            }else if(iShop.getShopArea().isInArea(event.getFrom())){
                if(!iShop.getShopArea().isInArea(event.getTo())){
                    iShop.onPlayerLeave(playerMoved);
                    Messaging.sendMessage(event.getPlayer(), "hub.shops." + iShop.getName() + ".leave");
                    if(iShop.hasMusic()) {
                        if (iShop.getJukebox() != null) {
                            iShop.getJukebox().removePlayer(playerMoved);
                            Hub.getInstance().getHubJukebox().addPlayer(playerMoved);
                        } else {
                            Log.error(LL.ERROR, MallCore.getInstance().getServer().getNickname(), "Player Movement -> getting Malljukebox", "MallJukebox is null", "MallJukebox is null while there was a music url given");
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onPlayerJoinEvent(PlayerJoinEvent event) {
        MallPlayer mallPlayer = MallPlayer.getPlayer(event.getPlayer().getUniqueId());
      event.getPlayer().teleport(Hub.getInstance().getHubSpawn());
        MallJukebox.sendPlayerLink(event.getPlayer());
        Hub.getInstance().getHubJukebox().addPlayer(event.getPlayer());
        Hub.getInstance().getAnnouncementsJukebox().addPlayer(event.getPlayer());
        HubPlayer.createPlayer(event.getPlayer().getUniqueId(), event.getPlayer().getName());
        HubPlayer hubPlayer = HubPlayer.getPlayer(event.getPlayer().getUniqueId());
        if(hubPlayer.getEquippedClothes() != null){
          ClothesProduct helmet = hubPlayer.getEquippedClothes().get(ClothingType.HELMET);
          ClothesProduct chest = hubPlayer.getEquippedClothes().get(ClothingType.CHESTPLATE);
          ClothesProduct legs = hubPlayer.getEquippedClothes().get(ClothingType.LEGGINGS);
          ClothesProduct boots = hubPlayer.getEquippedClothes().get(ClothingType.BOOTS);
          if(helmet != null)
            event.getPlayer().getInventory().setHelmet(helmet.getIcon());
          if(chest != null)
            event.getPlayer().getInventory().setChestplate(chest.getIcon());
          if(legs != null)
            event.getPlayer().getInventory().setLeggings(legs.getIcon());
          if(boots != null)
            event.getPlayer().getInventory().setBoots(boots.getIcon());
        }
        Hub.getInstance().getScoreboardManager().displayLobbyScorboard(MallPlayer.getPlayer(event.getPlayer().getUniqueId()));
        event.getPlayer().getInventory().clear();
        if(hubPlayer.getHotbarItemsHashMap() == null){
          hubPlayer.setHotbarItemsHashMap(new HashMap<>());
          hubPlayer.resetHotbar();
        }
        hubPlayer.sortPlayerVisibility();
        for(int i : hubPlayer.getHotbarItemsHashMap().keySet()){
          HotbarItems hotbarItem = hubPlayer.getHotbarItemsHashMap().get(i);
          if(hotbarItem != null){
            event.getPlayer().getInventory().setItem(i, hotbarItem.getItemForPlayer(MallPlayer.getPlayer(event.getPlayer().getUniqueId())));
          }
        }
        for(Player player : Bukkit.getOnlinePlayers()){
          MallPlayer mallPlayer1 = MallPlayer.getPlayer(player.getUniqueId());
          HubPlayer hubPlayer1 = HubPlayer.getPlayer(player.getUniqueId());
          if(hubPlayer1.getPlayerVisibility() == PlayerVisibility.VIEW_FRIENDS){
            if(mallPlayer1.getFriends() != null && !mallPlayer1.getFriends().contains(event.getPlayer().getUniqueId())){
              player.hidePlayer(event.getPlayer());
            }
          }else if(hubPlayer1.getPlayerVisibility() == PlayerVisibility.NONE){
            player.hidePlayer(event.getPlayer());
          }
        }
        Hub.getInstance().getBossBarManger().addPlayer(mallPlayer);
    }

    @EventHandler
    public void onPlayerDamage(EntityDamageEvent event){
      if(event.getEntity() instanceof Player){
        event.setCancelled(true);
      }
    }

    @EventHandler
    public void onPlayerLeaveEvent(PlayerQuitEvent event)
    {
        MallPlayer mallPlayer = MallPlayer.getPlayer(event.getPlayer().getUniqueId());
        if(Shops.getShop("Clothes").getShopArea().isInArea(event.getPlayer())){
        IShop shop = Shops.getShop("Clothes");
        ClothesShop clothesShop = (ClothesShop) shop;
        DressingRoom dressingRoom = clothesShop.isInRoom(event.getPlayer());
        if(dressingRoom!= null){
          dressingRoom.onLeave(MallPlayer.getPlayer(event.getPlayer().getUniqueId()));
        }
      }
        Hub.getInstance().getHubJukebox().removePlayer(event.getPlayer());
        Hub.getInstance().getAnnouncementsJukebox().removePlayer(event.getPlayer());
        Hub.getInstance().getBossBarManger().removePlayer(mallPlayer);
        HubPlayer hubPlayer = HubPlayer.getPlayer(event.getPlayer().getUniqueId());
        HubPlayerData.getInstance().savePlayer(hubPlayer);
        HubPlayer.removePlayer(event.getPlayer().getUniqueId());
    }
}
