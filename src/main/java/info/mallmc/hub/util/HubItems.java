package info.mallmc.hub.util;

import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.framework.util.Messaging;
import info.mallmc.framework.util.items.ItemUtil;
import java.util.Arrays;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class HubItems {

  public static ItemStack getEmptyCup(MallPlayer mallPlayer)
  {
    return ItemUtil.getSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZmI4MDk4ODlkYTM2YWRmNTYxODU4MWJiZjdiNjZkMmQ4ODM5ZTJlYjcyNTRjMzMzMmU0ZjNhMjMwZmEifX19", Messaging.getMessage(mallPlayer.getLanguageIdentifier(), "hub.items.emptycup.name"),
        Arrays.asList(Messaging.getMessage(mallPlayer.getLanguageIdentifier(), "hub.items.emptycup.lore")));
  }

  public static ItemStack[] getPlayerInventory(MallPlayer mallPlayer){
    ItemStack[] hotbar = new ItemStack[9];

    return hotbar;
  }

}
