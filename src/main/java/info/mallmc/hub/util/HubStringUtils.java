package info.mallmc.hub.util;

import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.framework.util.Messaging;

public class HubStringUtils
{
    public static String replacePlayerInfoInString(String string, MallPlayer mallPlayer)
    {
        string = string.replace("{chat_prefix}", mallPlayer.getChatPrefix());
        string = string.replace("{name}", mallPlayer.getUsername());
        string = string.replace("{chat_suffix}", mallPlayer.getChatSuffix());
        string = string.replace("{laser_server}", mallPlayer.getLastServer());
        string = string.replace("{rank_prefix}", mallPlayer.getRankPrefix());
        string = string.replace("{rank_color}", mallPlayer.getRank().getColor().toString());
        string = string.replace("{rank_name}", mallPlayer.getRank().getName());
        string = string.replace("{permissionSet_name}", mallPlayer.getPermissionSet().name());
        string = string.replace("{chatChannel_name}", mallPlayer.getChatChannel().getChannelName());
        string = string.replace("{currency}", mallPlayer.getCurrency() + "");
        string = string.replace("{xp_raw}", mallPlayer.getXp() + "");
        string = string.replace("{xp_level}", mallPlayer.getLevel() + "");
        string = string.replace("{play_time}", mallPlayer.getPlayTime() + "");
        string = string.replace("{languageIdentifier_name}", mallPlayer.getLanguageIdentifier().getDisplayName());
        string = Messaging.colorizeMessage(string);
        return string;

    }
}
