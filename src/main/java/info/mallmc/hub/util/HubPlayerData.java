package info.mallmc.hub.util;

import info.mallmc.core.MallCore;
import info.mallmc.hub.api.HubPlayer;
import java.util.List;
import java.util.UUID;
import org.mongodb.morphia.Datastore;

public class HubPlayerData {

  private static HubPlayerData playerData;

  public static HubPlayerData getInstance() {
    if (playerData == null) {
      playerData = new HubPlayerData();
    }
    return playerData;
  }

  private final Datastore datastore = MallCore.getInstance().getDatabase().getDatastore();

  /**
   * Does the player exist in the database?
   *
   * @param uuid The UUID of the player to find
   * @return Whether or not the player exists in the database
   */
  public synchronized boolean isInDatabase(UUID uuid) {
    final List<HubPlayer> players = datastore.createQuery(HubPlayer.class).field("uuid")
        .equal(uuid).asList();
    if (players.size() > 0 && players.size() == 1) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Get a player by their UUID
   *
   * @param uuid The UUID of the player to get
   * @return The actual player in the database
   */
  public synchronized HubPlayer getPlayerByUUID(UUID uuid) {
    final List<HubPlayer> players = datastore.createQuery(HubPlayer.class).field("uuid")
        .equal(uuid).asList();
    if (players.size() > 0 && players.size() == 1) {
      return players.get(0);
    } else {
      return null;
    }
  }

  /**
   * Save the player to the database
   *
   * @param hubPlayer The player to save to the database
   */
  public synchronized void savePlayer(HubPlayer hubPlayer) {
    final List<HubPlayer> players = datastore.createQuery(HubPlayer.class).field("uuid").equal(hubPlayer.getUuid()).asList();
    if (players == null || players.size() < 1) {
      datastore.save(hubPlayer);
      return;
    }
    HubPlayer savedPlayer = players.get(0);
    hubPlayer.setId(savedPlayer.getId());
    datastore.save(hubPlayer);
  }
}
