package info.mallmc.hub.util;

import info.mallmc.core.MallCore;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.framework.util.Messaging;
import info.mallmc.framework.util.items.ItemUtil;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public enum  HotbarItems {

  NAVIGATION("hub.items.navigation.name", "hub.items.navigation.lore", Material.COMPASS),
  PLAYER_INFO("hub.items.player.name", "hub.items.player.lore", Material.SKULL_ITEM);

  private String nameKey;
  private String loreKey;
  private Material material;

  HotbarItems(String nameKey, String loreKey, Material material) {
    this.nameKey = nameKey;
    this.loreKey = loreKey;
    this.material = material;
  }

  public ItemStack getItemForPlayer(MallPlayer mallPlayer){
    List<String> strings =  MallCore.getInstance().getLanguageMap().get(mallPlayer.getLanguageIdentifier()).getStrings(loreKey);
    for(String string : strings){
      strings.set(strings.indexOf(string), Messaging.colorizeMessage(string));
    }
    if(material == Material.SKULL_ITEM){
      return ItemUtil.getSkullPlayer(Bukkit.getPlayer(mallPlayer.getUuid()), Messaging.getMessage(mallPlayer.getLanguageIdentifier(), nameKey), strings);
    }
    return ItemUtil.createItem(material, Messaging.getMessage(mallPlayer.getLanguageIdentifier(), nameKey), strings);
  }

  public Material getMaterial() {
    return material;
  }
}
