package info.mallmc.hub;

import info.mallmc.core.api.GameRules;
import info.mallmc.framework.events.custom.FrameworkEnableEvent;
import info.mallmc.framework.util.MallCommand;
import info.mallmc.framework.util.jukebox.MallJukebox;
import info.mallmc.framework.util.jukebox.MallMedia;
import info.mallmc.hub.annoucements.AnnouncementHandler;
import info.mallmc.hub.command.AnnoucementCommand;
import info.mallmc.hub.command.CommandShop;
import info.mallmc.hub.command.ScoreboardCommand;
import info.mallmc.hub.events.HubInteractions;
import info.mallmc.hub.events.HubPlayerEvents;
import info.mallmc.hub.events.core.HubCoreEvents;
import info.mallmc.hub.events.framework.EventPlayerUpdate;
import info.mallmc.hub.managers.BossBarManager;
import info.mallmc.hub.managers.ScoreboardManager;
import info.mallmc.hub.shops.Shops;
import info.mallmc.hub.shops.airship.AirShipShop;
import info.mallmc.hub.shops.clothes.ClothesShop;
import info.mallmc.hub.shops.coffee.CoffeeShop;
import info.mallmc.hub.shops.icecream.IceCreamShop;
import info.mallmc.hub.shops.laser.LaserShop;
import info.mallmc.hub.shops.pet.PetShop;
import info.mallmc.hub.shops.toy.ToyShop;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.boss.BossBar;
import org.bukkit.event.EventHandler;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Hub extends JavaPlugin {

  private static Hub instance;

  public static Hub getInstance() {
    return instance;
  }

  private Location hubSpawn;
  private World hubWorld;

  private PluginManager pluginManager;
  private AnnouncementHandler announcementHandler;
  private ScoreboardManager scoreboardManger;
  private BossBarManager bossBarManger;

  private MallJukebox hubJukebox;
  private MallJukebox announcementsJukebox;

  //Shops
  private CoffeeShop coffeeShop;
  private ToyShop toyShop;
  private AirShipShop airShipShop;
  private LaserShop laserShop;
  private PetShop petShop;
  private IceCreamShop iceCreamShop;
  private ClothesShop clothesShop;

  @Override
  public void onEnable() {
    instance = this;
    hubWorld = Bukkit.getWorlds().get(0);
    pluginManager = Bukkit.getPluginManager();
    scoreboardManger = new ScoreboardManager();
    bossBarManger = new BossBarManager();

    hubWorld.setDifficulty(Difficulty.EASY);
    hubWorld.setGameRuleValue(GameRules.DO_MOB_SPAWNING.getName(), "false");

    hubSpawn = new Location(hubWorld, -17, 61, -44);

    //Resister Commands Here
    MallCommand.registerCommand("hub", "shop", new CommandShop());
    MallCommand.registerCommand("hub", "announcement", new AnnoucementCommand());
    MallCommand.registerCommand("hub", "sb", new ScoreboardCommand());

    //Register Event Here
    pluginManager.registerEvents(new HubPlayerEvents(), this);
    new HubCoreEvents();

    announcementHandler = new AnnouncementHandler();

    //Make Shops Here
    coffeeShop = new CoffeeShop();
    toyShop = new ToyShop();
    airShipShop = new AirShipShop();
    laserShop = new LaserShop();
    petShop = new PetShop();
    iceCreamShop = new IceCreamShop();
    clothesShop = new ClothesShop();
    //Register Shops Here
    Shops.registerShop(coffeeShop);
    Shops.registerShop(toyShop);
    Shops.registerShop(airShipShop);
    Shops.registerShop(laserShop);
    Shops.registerShop(petShop);
    Shops.registerShop(iceCreamShop);
    Shops.registerShop(clothesShop);

    HubInteractions.registerInteractions();
    new EventPlayerUpdate();


    this.hubJukebox = new MallJukebox("hub_lobby");
    this.hubJukebox.playSoundForShow(new MallMedia("http://cdn.redstone.tech/Rushmeadfiles/mallmc/mcjukebox/hub/lobby/lobby1.mp3", true));

    this.announcementsJukebox = new MallJukebox("hub_announcements");

  }

  @EventHandler
  public void onFrameworkEnable(FrameworkEnableEvent enableEvent) {

  }

  @Override
  public void onDisable(){

  }

  public Location getHubSpawn() {
    return hubSpawn;
  }

  public World getHubWorld() {
    return hubWorld;
  }

  public MallJukebox getHubJukebox() {
    return hubJukebox;
  }

  public MallJukebox getAnnouncementsJukebox() {
    return announcementsJukebox;
  }

  public AnnouncementHandler getAnnouncementHandler() {
    return announcementHandler;
  }

  public ScoreboardManager getScoreboardManager() {
    return scoreboardManger;
  }

  public BossBarManager getBossBarManger() {
    return bossBarManger;
  }

  public PluginManager getPluginManager() {
    return pluginManager;
  }
}
