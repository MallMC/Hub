package info.mallmc.hub.inventories;

import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.util.TextUtils;
import info.mallmc.framework.api.MallInventory;
import info.mallmc.framework.util.Messaging;
import info.mallmc.framework.util.items.ItemUtil;
import info.mallmc.hub.inventories.settings.PlayerSettingsInventory;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class PlayerInformationInventory extends MallInventory{

  Player player;
  MallPlayer mallPlayer;

  public PlayerInformationInventory(Player player) {
    super(MallPlayer.getPlayer(player.getUniqueId()), "hub.items.player.name", 9);
    this.player = player;
    this.mallPlayer = MallPlayer.getPlayer(player.getUniqueId());
  }

  @Override
  protected void populate() {
    {
      List<String> playerLore = getLore(mallPlayer, "hub.stats.player.lore");
      playerLore.set(0, TextUtils.replace(playerLore.get(0), player.getUniqueId().toString()));
      ItemStack playerHead = ItemUtil.getSkullPlayer(player, mallPlayer.getRank().getColor() + player.getName(), playerLore);
      this.inventory.setItem(0, playerHead);
    }
    {
      List<String> playerLore = getLore(mallPlayer, "hub.stats.rank.lore");
      playerLore.set(0, TextUtils.replace(playerLore.get(0), mallPlayer.getRank().getColor(), mallPlayer.getRank().getName()));
      ItemStack playerHead = ItemUtil.createItem(Material.IRON_INGOT, TextUtils.replace(getMessage(mallPlayer, "hub.stats.rank.name")), playerLore);
      this.inventory.setItem(1, playerHead);
    }
    {
      List<String> playerLore = getLore(mallPlayer, "hub.stats.coins.lore");
      playerLore.set(0, TextUtils.replace(playerLore.get(0), mallPlayer.getCurrency()));
      ItemStack playerHead = ItemUtil.createItem(Material.GOLD_INGOT, TextUtils.replace(getMessage(mallPlayer, "hub.stats.coins.name")), playerLore);
      this.inventory.setItem(2, playerHead);
    }
    {
      List<String> playerLore = getLore(mallPlayer, "hub.stats.time.lore");
      long playSecs = mallPlayer.getPlayTime();
      String playTime = "";
      if (TimeUnit.MILLISECONDS.toDays(playSecs) > 0) {
        long days = TimeUnit.MILLISECONDS.toDays(playSecs);
        playTime += days + "d ";
        playSecs -= (days * 60 * 60 * 24) * 1000;
      }
      if (TimeUnit.MILLISECONDS.toHours(playSecs) > 0) {
        long hours = TimeUnit.MILLISECONDS.toHours(playSecs);
        playTime += hours + "h ";
        playSecs -= (hours * 60 * 60) * 1000;
      }
      if (TimeUnit.MILLISECONDS.toMinutes(playSecs) > 0) {
        long mins = TimeUnit.MILLISECONDS.toMinutes(playSecs);
        playTime += mins + "m ";
        playSecs -= (mins * 60) * 1000;
      }
      if (TimeUnit.MILLISECONDS.toSeconds(playSecs) > 0) {
        long seconds = TimeUnit.MILLISECONDS.toSeconds(playSecs);
        playTime += seconds + "s";
      }
      playerLore.set(0, TextUtils.replace(playerLore.get(0), playTime));
      ItemStack playerHead = ItemUtil.createItem(Material.WATCH, TextUtils.replace(getMessage(mallPlayer, "hub.stats.time.name")), playerLore);
      this.inventory.setItem(3, playerHead);
    }
    {
      List<String> playerLore = getLore(mallPlayer, "hub.stats.level.lore");
      playerLore.set(0, TextUtils.replace(playerLore.get(0), mallPlayer.getLevel()));
      ItemStack playerHead = ItemUtil.createStack(Material.EXP_BOTTLE, mallPlayer.getLevel(), TextUtils.replace(getMessage(mallPlayer, "hub.stats.level.name")), playerLore);
      this.inventory.setItem(4, playerHead);
    }
    {
      ItemStack settings = ItemUtil.createItem(Material.REDSTONE_COMPARATOR, getMessage(mallPlayer, "hub.stats.settings.name"), getLore(mallPlayer, "hub.stats.settings.lore"));
      this.inventory.setItem(8, settings);
    }
  }

  @Override
  protected void onPlayerClick(InventoryClickEvent inventoryClickEvent) {
    if(inventoryClickEvent.getRawSlot() == 8){
      close();
      new PlayerSettingsInventory(player).open(player);
    }
  }
}
