package info.mallmc.hub.inventories.settings;

import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.framework.api.MallInventory;
import info.mallmc.framework.util.items.FrameworkItems;
import info.mallmc.framework.util.items.ItemUtil;
import info.mallmc.hub.api.HubPlayer;
import info.mallmc.hub.api.HubPlayer.PlayerVisibility;
import info.mallmc.hub.inventories.PlayerInformationInventory;
import info.mallmc.hub.util.HotbarItems;
import java.util.HashMap;
import java.util.List;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Dye;

public class HotBarLayoutInventory extends MallInventory {

  private Player player;
  private HashMap<Integer, HotbarItems> hotbarItems;

  public HotBarLayoutInventory(Player player) {
    super(MallPlayer.getPlayer(player.getUniqueId()), "hub.settings.hotbar.name", 18);
    this.player = player;
    this.hotbarItems = HubPlayer.getPlayer(player.getUniqueId()).getHotbarItemsHashMap();
  }


  @Override
  protected void populate() {
    MallPlayer mallPlayer = MallPlayer.getPlayer(player.getUniqueId());
    HubPlayer hubPlayer = HubPlayer.getPlayer(player.getUniqueId());
    {
      ItemStack empty = ItemUtil.createItem(Material.WEB, getMessage(mallPlayer, "hub.settings.hotbar.empty.name"));
      for(int i = 0; i < 9; i++){
        if(hotbarItems.containsKey(i)){
          this.inventory.setItem(i, hotbarItems.get(i).getItemForPlayer(mallPlayer));
        }else{
          this.inventory.setItem(i, empty);
        }
      }
    }
    {
      ItemStack back = ItemUtil.createItem(Material.BARRIER, getMessage(mallPlayer, "hub.items.reset"));
      this.inventory.setItem(16, back);
    }
    {
      ItemStack back = ItemUtil.createItem(Material.ARROW, getMessage(mallPlayer, "hub.items.back"));
      this.inventory.setItem(17, back);
    }

  }

  @Override
  protected void onPlayerClick(InventoryClickEvent inventoryClickEvent) {
    int slot = inventoryClickEvent.getRawSlot();
    MallPlayer mallPlayer = MallPlayer.getPlayer(player.getUniqueId());
    HubPlayer hubPlayer = HubPlayer.getPlayer(player.getUniqueId());
    if(slot > inventory.getSize()){
      return;
    }
    if(slot == 17){
      player.getInventory().clear();
      for(int i  : hubPlayer.getHotbarItemsHashMap().keySet()){
        HotbarItems hotbarItem = hubPlayer.getHotbarItemsHashMap().get(i);
        if(hotbarItem != null){
          player.getInventory().setItem(i, hotbarItem.getItemForPlayer(mallPlayer));
        }
      }
      if(!hubPlayer.getHotbarItemsHashMap().containsValue(HotbarItems.PLAYER_INFO)){
        for(int i  = 0; i < 9; i++){
          if(player.getInventory().getItem(i) == null){
            player.getInventory().setItem(i, HotbarItems.PLAYER_INFO.getItemForPlayer(mallPlayer));
            hubPlayer.getHotbarItemsHashMap().put(i, HotbarItems.PLAYER_INFO);
            break;
          }
        }
      }
      player.updateInventory();
      close();
      new PlayerInformationInventory(player).open(player);
    }else if(slot == 16) {
      hubPlayer.resetHotbar();
      this.hotbarItems = hubPlayer.getHotbarItemsHashMap();
      repopulate();
    }else if(slot < 9){
      close();
      new HotbarItemSelectInventory(player, slot).open(player);
    }
  }
}
