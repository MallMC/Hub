package info.mallmc.hub.inventories.settings;

import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.util.TextUtils;
import info.mallmc.framework.api.MallInventory;
import info.mallmc.framework.util.items.FrameworkItems;
import info.mallmc.framework.util.items.ItemUtil;
import info.mallmc.hub.api.HubPlayer;
import info.mallmc.hub.api.HubPlayer.PlayerVisibility;
import info.mallmc.hub.inventories.PlayerInformationInventory;
import java.util.List;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Dye;

public class PlayerSettingsInventory extends MallInventory {

  private Player player;

  public PlayerSettingsInventory(Player player) {
    super(MallPlayer.getPlayer(player.getUniqueId()), "hub.items.player.name", 9);
    this.player = player;
  }


  @Override
  protected void populate() {
    MallPlayer mallPlayer = MallPlayer.getPlayer(player.getUniqueId());
    HubPlayer hubPlayer = HubPlayer.getPlayer(player.getUniqueId());
    // View Players
    {
      ItemStack playerVisibility = null;
      Dye dye;
      if(hubPlayer.getPlayerVisibility() == null){
        hubPlayer.setPlayerVisibility(PlayerVisibility.VIEW_ALL);
      }
      switch (hubPlayer.getPlayerVisibility()) {
        case NONE:
          playerVisibility = ItemUtil.createItem(Material.STAINED_GLASS_PANE, DyeColor.RED.getWoolData(),
              getMessage(mallPlayer, "hub.settings.visibility.names.off"),
              getLore(mallPlayer, "hub.settings.visibility.lores.off"));
          break;
        case VIEW_ALL:
          playerVisibility = ItemUtil.createItem(Material.STAINED_GLASS_PANE, DyeColor.GREEN.getWoolData(),
              getMessage(mallPlayer, "hub.settings.visibility.names.all"),
              getLore(mallPlayer, "hub.settings.visibility.lores.all"));
          break;
        case VIEW_FRIENDS:
          playerVisibility = ItemUtil.createItem(Material.STAINED_GLASS_PANE, DyeColor.ORANGE.getWoolData(),
              getMessage(mallPlayer, "hub.settings.visibility.names.friends"),
              getLore(mallPlayer, "hub.settings.visibility.lores.friends"));
          break;
      }
      this.inventory.setItem(0, playerVisibility);
    }
    // Language
    {
      List<String> playerLore = getLore(mallPlayer, "hub.settings.language.lore");
      playerLore.set(0, TextUtils.replace(playerLore.get(0), mallPlayer.getLanguageIdentifier().getDisplayName()));
      ItemStack lang = ItemUtil.createItem(Material.NAME_TAG, getMessage(mallPlayer, "hub.settings.language.name"), playerLore);
      this.inventory.setItem(1, lang);
    }
    // Friend Requests
    {
      boolean friendRequests = mallPlayer.isFriendRequestsEnabled();
      ItemStack friendRequestStack;
      if(friendRequests){
        friendRequestStack = ItemUtil.createItem(Material.STAINED_CLAY, DyeColor.GREEN.getWoolData(), getMessage(mallPlayer, "hub.settings.friendRequests.names.on"), getLore(mallPlayer, "hub.settings.friendRequests.lores.on"));
      }else{
        friendRequestStack = ItemUtil.createItem(Material.STAINED_CLAY, DyeColor.RED.getWoolData(), getMessage(mallPlayer, "hub.settings.friendRequests.names.off"), getLore(mallPlayer, "hub.settings.friendRequests.lores.off"));
      }
      this.inventory.setItem(2, friendRequestStack);
    }
    //Hotbar Organisation
    {
      ItemStack hotbar = ItemUtil.createItem(Material.PAPER, getMessage(mallPlayer, "hub.settings.hotbar.name"), getLore(mallPlayer, "hub.settings.hotbar.lore"));
      this.inventory.setItem(3, hotbar);
    }
    {
      ItemStack back = ItemUtil.createItem(Material.ARROW, getMessage(mallPlayer, "hub.items.back"));
      this.inventory.setItem(8, back);
    }

  }

  @Override
  protected void onPlayerClick(InventoryClickEvent inventoryClickEvent) {
    int slot = inventoryClickEvent.getRawSlot();
    MallPlayer mallPlayer = MallPlayer.getPlayer(player.getUniqueId());
    HubPlayer hubPlayer = HubPlayer.getPlayer(player.getUniqueId());
    if(slot == 0){
      switch (hubPlayer.getPlayerVisibility()) {
        case NONE:
          hubPlayer.setPlayerVisibility(PlayerVisibility.VIEW_ALL);
          break;
        case VIEW_ALL:
          hubPlayer.setPlayerVisibility(PlayerVisibility.VIEW_FRIENDS);
          break;
        case VIEW_FRIENDS:
          hubPlayer.setPlayerVisibility(PlayerVisibility.NONE);
          break;
      }
      repopulate();
      hubPlayer.sortPlayerVisibility();
    }
    if(slot == 2){
      mallPlayer.setFriendRequestsEnabled(!mallPlayer.isFriendRequestsEnabled());
      repopulate();
    }
    if(slot == 3){
      close();
      new HotBarLayoutInventory(player).open(player);
    }
    if(slot == 8){
      hubPlayer.resetHotbar();
      close();
      new PlayerInformationInventory(player).open(player);
    }
  }
}
