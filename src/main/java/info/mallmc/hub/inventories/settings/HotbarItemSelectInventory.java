package info.mallmc.hub.inventories.settings;

import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.framework.api.MallInventory;
import info.mallmc.framework.util.items.ItemUtil;
import info.mallmc.hub.api.HubPlayer;
import info.mallmc.hub.inventories.PlayerInformationInventory;
import info.mallmc.hub.util.HotbarItems;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class HotbarItemSelectInventory extends MallInventory {

  private Player player;
  private int slot;
  private List<HotbarItems> hotbarItemsList;

  public HotbarItemSelectInventory(Player player, int slot) {
    super(MallPlayer.getPlayer(player.getUniqueId()), "hub.settings.hotbar.select", 9);
    this.player = player;
    this.slot = slot;
    this.hotbarItemsList = new ArrayList<>();
  }


  @Override
  protected void populate() {
    MallPlayer mallPlayer = MallPlayer.getPlayer(player.getUniqueId());
    HubPlayer hubPlayer = HubPlayer.getPlayer(player.getUniqueId());
    ItemStack empty = ItemUtil.createItem(Material.WEB, getMessage(mallPlayer, "hub.settings.hotbar.empty.name"));
    {
      int lastSlot = 0;
      for(HotbarItems hotbarItems : HotbarItems.values()){
        if(!hubPlayer.getHotbarItemsHashMap().containsValue(hotbarItems)){
          this.inventory.setItem(lastSlot, hotbarItems.getItemForPlayer(mallPlayer));
          hotbarItemsList.add(lastSlot, hotbarItems);
          lastSlot++;
        }
      }
      this.inventory.setItem(7, empty);
    }
    {
      ItemStack back = ItemUtil.createItem(Material.ARROW, getMessage(mallPlayer, "hub.items.back"));
      this.inventory.setItem(8, back);
    }

  }

  @Override
  protected void onPlayerClick(InventoryClickEvent inventoryClickEvent) {
    int slot = inventoryClickEvent.getRawSlot();
    MallPlayer mallPlayer = MallPlayer.getPlayer(player.getUniqueId());
    HubPlayer hubPlayer = HubPlayer.getPlayer(player.getUniqueId());

    if(slot == 8){
      close();
      new HotBarLayoutInventory(player).open(player);
    }else{
      if(slot > hotbarItemsList.size()){
        HashMap<Integer, HotbarItems> hashMap = hubPlayer.getHotbarItemsHashMap();
        hashMap.remove(this.slot);
        hubPlayer.setHotbarItemsHashMap(hashMap);
        close();
        new HotBarLayoutInventory(player).open(player);
        return;
      }
      HotbarItems hotbarItems = hotbarItemsList.get(slot);
      if(hotbarItems == null){
        return;
      }
      hubPlayer.getHotbarItemsHashMap().computeIfPresent(this.slot, (key, value) -> hubPlayer.getHotbarItemsHashMap().replace(key, hotbarItems));
      hubPlayer.getHotbarItemsHashMap().computeIfAbsent(this.slot, (key) -> hubPlayer.getHotbarItemsHashMap().put(key, hotbarItems));
      close();
      new HotBarLayoutInventory(player).open(player);
    }
  }
}
