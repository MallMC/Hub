package info.mallmc.hub.inventories;


import info.mallmc.core.MallCore;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.framework.api.MallInventory;
import info.mallmc.framework.util.Messaging;
import info.mallmc.framework.util.items.ItemUtil;
import info.mallmc.hub.Hub;
import java.util.List;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class NavigationInventory extends MallInventory {

  private Player player;

  public NavigationInventory(Player player){
    super(MallPlayer.getPlayer(player.getUniqueId()), "hub.items.navigation.name", 54);
    this.player = player;
  }


  @Override
  protected void populate() {
    MallPlayer mallPlayer = MallPlayer.getPlayer(player.getUniqueId());
    ItemStack comingSoon = ItemUtil
        .createItem(Material.IRON_FENCE, getMessage(mallPlayer, "hub.navigation.names.soon"), getLore(mallPlayer, "hub.navigation.lores.soon"));
    ItemStack spawn = ItemUtil.createItem(Material.WEB, getMessage(mallPlayer, "hub.navigation.names.spawn"), getLore(mallPlayer, "hub.navigation.lores.spawn"));
    ItemStack parkour = ItemUtil.createItem(Material.FEATHER, getMessage(mallPlayer, "hub.navigation.names.parkour"), getLore(mallPlayer, "hub.navigation.lores.parkour"));
    ItemStack coffee = ItemUtil.getSkull(
        "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZTFjZWRkNTdlYzFiM2FjMTQ1NDQ2MjZjYzZiNGJjZGJkYzM1MTNmMzlhOTFjYzM3YTA0OGE5ZmQyNDRkNGQifX19",
        getMessage(mallPlayer, "hub.navigation.names.coffee"), getLore(mallPlayer, "hub.navigation.lores.coffee"));
//    ItemStack iceCreamStore = Shops.getShop("IceCream").BowlOfIceCream.getIceCreamProduct()
//        .getItemStack(mallPlayer);
//    ItemMeta itemMeta = iceCreamStore.getItemMeta();
//    itemMeta.setDisplayName(Messaging.colorizeMessage("&aIce-Cream Store"));
//    itemMeta.getLore().clear();
//    itemMeta.getLore().add(Messaging.colorizeMessage("&6Go to the ice-cream store"));
//    iceCreamStore.setItemMeta(itemMeta);
    ItemStack toyStore = ItemUtil.getSkull(
        "eyJ0aW1lc3RhbXAiOjE1MDIwMTU5OTM2NjcsInByb2ZpbGVJZCI6ImQ5MzUxNTFlNDM2MTRlNDE4ZTQyZGQ4MGUxYWY1MzFlIiwicHJvZmlsZU5hbWUiOiJSaWNoYXJkMTIzMCIsInRleHR1cmVzIjp7IlNLSU4iOnsidXJsIjoiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS84ZjkyMzNjMTI0N2UwM2U5ZmQyNzc0MjczN2U3OWU0Y2NlYmQyMjVhOWIwNTlkNTk2ZDVjZDM0ZTI2ZjIxNjUifX19",
        getMessage(mallPlayer, "hub.navigation.names.toyStore"), getLore(mallPlayer, "hub.navigation.lores.toyStore"));
    ItemStack laserQuest = ItemUtil.getSkull(
        "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYjc3NWExN2MyOTQxYWU2YTJhNWYxODQwNTA5YjlhYjBjMGQ5Njg1OWE5YmMyNDk3OThiODZmMTk1MmIwODMyZSJ9fX0=",
        getMessage(mallPlayer, "hub.navigation.names.laserQuest"), getLore(mallPlayer, "hub.navigation.lores.laserQuest"));
    ItemStack airShipWars = ItemUtil.getSkull(
        "eyJ0aW1lc3RhbXAiOjE1MDIwMTY0NzYwMTEsInByb2ZpbGVJZCI6IjgxNDlkYzI4NDViYzQ5M2VhNDliY2JmMWU5ZjIyN2JhIiwicHJvZmlsZU5hbWUiOiJzYW1zYW1zYW0xMjM0IiwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlL2ViNTllY2MzYTRiOTI4OTI2ZmIzYWQyY2FkYjM0ODYyNjc5NzM4YTJlNjZhNjhjMTZhYjhhM2ExYTY1YiJ9fX0=",
        getMessage(mallPlayer, "hub.navigation.names.airshipWars"),
        getLore(mallPlayer, "hub.navigation.lores.airshipWars"));
    this.inventory.setItem(4, spawn);
    ItemStack shops = ItemUtil.createItem(Material.WOOD, getMessage(mallPlayer, "hub.navigation.names.shops"));
    for (int i = 0; i < 9; i++) {
      int pos = 9 + i;
      this.inventory.setItem(pos, shops);
    }
    this.inventory.setItem(21, comingSoon);
    this.inventory.setItem(22, coffee);
    this.inventory.setItem(23, toyStore);
    ItemStack games = ItemUtil.createItem(Material.WOOD, getMessage(mallPlayer, "hub.navigation.names.games"));
    for (int i = 0; i < 9; i++) {
      int pos = 27 + i;
      this.inventory.setItem(pos, games);
    }
    this.inventory.setItem(39, airShipWars);
    this.inventory.setItem(40, laserQuest);
    this.inventory.setItem(41, comingSoon);
    ItemStack seperator = ItemUtil.createItem(Material.WOOD, "");
    for (int i = 0; i < 9; i++) {
      int pos = 45 + i;
      this.inventory.setItem(pos, seperator);
    }
  }

  @Override
  public void onPlayerClick(InventoryClickEvent event) {
    if (event.getRawSlot() == 22) {
      player.teleport(new Location(player.getWorld(), 57, 61, -34));
    }
//    if (event.getRawSlot() == 21) {
//      player.teleport(new Location(player.getWorld(), 75, 71, -57, 179, 0));
//    }
    if (event.getRawSlot() == 23) {
      player.teleport(new Location(player.getWorld(), 57, 71, -29));
    }
    if (event.getRawSlot() == 40) {
      player.teleport(new Location(player.getWorld(), 75, 61, -32));
    }
    if (event.getRawSlot() == 39) {
      player.teleport(new Location(player.getWorld(), 57, 71, -57, 180, 0));
    }
    if (event.getRawSlot() == 4) {
      player.teleport(Hub.getInstance().getHubSpawn());
    }
  }

}