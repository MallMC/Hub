package info.mallmc.hub.command;

import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.framework.api.IMallExecutor;
import info.mallmc.framework.util.MallCommand;
import info.mallmc.hub.Hub;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ScoreboardCommand implements IMallExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        Player player = (Player) commandSender;
        Hub.getInstance().getScoreboardManager().displayLobbyScorboard(MallPlayer.getPlayer(player.getUniqueId()));
        return false;
    }

    @Override
    public MallCommand.SenderType getSenderType() {
        return MallCommand.SenderType.PLAYER;
    }
}
