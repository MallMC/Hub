package info.mallmc.hub.command;

import info.mallmc.core.api.player.PermissionSet;
import info.mallmc.framework.api.IMallExecutor;
import info.mallmc.framework.util.Messaging;
import info.mallmc.hub.Hub;
import info.mallmc.hub.annoucements.Announcement;
import info.mallmc.hub.annoucements.AnnouncementHandler;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class AnnoucementCommand implements IMallExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (args.length == 0) {
            Messaging.sendMessage(commandSender, "hub.commands.announcement.usage");
            return false;
        }
        String args1 = args[0];
        switch (args1.toLowerCase()) {
            case "reload":
                Messaging.sendMessage(commandSender, "hub.commands.announcement.reloaded");
                return true;
            case "force":
                if (args.length == 1) {
                    Messaging.sendMessage(commandSender, "hub.commands.announcement.usageforce");
                    return false;
                }
                String args2 = args[1];
                for (Announcement announcement : Hub.getInstance().getAnnouncementHandler().getAnncoucemnets()) {
                    if (announcement.getName().equalsIgnoreCase(args2)) {
                        announcement.triggerAnnouncement();
                        return true;
                    }
                }
                return false;
            default:
                Messaging.sendMessage(commandSender, "hub.commands.announcement.usage");
                return false;
        }
    }

    @Override
    public PermissionSet getNeedPermissionSet() {
        return PermissionSet.JNR_STAFF;
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] args) {
        switch (args.length) {
            case 1:
                return StringUtil.copyPartialMatches(args[0], Arrays.asList("force", "reload"), new ArrayList<>());
            case 2:
                    List<String> aNames = new ArrayList<>();
                    for (Announcement announcement : Hub.getInstance().getAnnouncementHandler().getAnncoucemnets()) {
                        aNames.add(announcement.getName());
                    }
                    return StringUtil.copyPartialMatches(args[1], aNames, new ArrayList<>());
            default:
                return null;
        }
    }
}
