package info.mallmc.hub.command;

import info.mallmc.core.api.player.PermissionSet;
import info.mallmc.framework.api.IMallExecutor;
import info.mallmc.framework.util.MallCommand.SenderType;
import info.mallmc.framework.util.Messaging;
import info.mallmc.hub.api.shop.EnumShopState;
import info.mallmc.hub.api.shop.IShop;
import info.mallmc.hub.shops.Shops;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.util.StringUtil;

public class CommandShop implements IMallExecutor {

  @Override
  public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
    if (args.length == 0) {
      Messaging.sendMessage(commandSender, "hub.commands.shop.usage");
      return false;
    } else {
      String args1 = args[0];
      for (IShop iShop : Shops.getRegisteredShops().values()) {
        if (iShop.getName().equalsIgnoreCase(args1)) {
          if (args.length >= 2) {
            String args2 = args[1];
            for(EnumShopState shopState: EnumShopState.values()) {
                if(shopState.name().equalsIgnoreCase(args2)) {
                  if(iShop.setShopState(shopState)) {
                    Messaging.sendMessage(commandSender, "hub.commands.shop.setstate." + shopState.name().toLowerCase());
                    return true;
                  } else {
                    Messaging.sendMessage(commandSender, "hub.commands.shop.samestate", shopState.name().toLowerCase());
                    return false;
                  }
                }
            }
            Messaging.sendMessage(commandSender, "hub.commands.shop.notastauts");
            return false;
          } else {
            String spot2 = "";
            for(EnumShopState shopState: EnumShopState.values()) {
                spot2 = spot2 + shopState.name().toLowerCase() + "/";
            }
            Messaging.sendMessage(commandSender, "hub.commands.shop.usage2", args1, spot2);
            return false;
          }
        }
      }
      Messaging.sendMessage(commandSender, "hub.commands.shop.errorShopName", args1);
      return false;
    }
  }

  @Override
  public SenderType getSenderType() {
    return SenderType.BOTH;
  }

  @Override
  public PermissionSet getNeedPermissionSet() {
    return PermissionSet.JNR_STAFF;
  }

  @Override
  public List<String> onTabComplete(CommandSender commandSender, Command command, String s,
      String[] args) {
    switch (args.length) {
      case 1:
        List<String> argsList = new ArrayList<>();
        for (IShop iShop : Shops.getRegisteredShops().values()) {
          argsList.add(iShop.getName());
        }
        return StringUtil.copyPartialMatches(args[0], argsList, new ArrayList<>());
      case 2:
        List<String> types = new ArrayList<>();
        for(EnumShopState shopState: EnumShopState.values()) {
          types.add(shopState.name().toLowerCase());
        }
        return StringUtil.copyPartialMatches(args[1], types, new ArrayList<>());
      default:
        return null;
    }
  }
}
