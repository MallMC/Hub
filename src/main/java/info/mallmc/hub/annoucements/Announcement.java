package info.mallmc.hub.annoucements;

import info.mallmc.core.MallCore;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.framework.util.Messaging;
import info.mallmc.framework.util.helpers.MathHelper;
import info.mallmc.framework.util.jukebox.MallMedia;
import info.mallmc.hub.Hub;
import org.bukkit.Bukkit;

import java.util.List;

public class Announcement
{
    private String name;
    private boolean hasSound;
    private int soundCount;
    private List<AnnouncementMessage> annoucemtMessages;



    public void triggerAnnouncement() {
        if (hasSound) {
            int soundNumber = MathHelper.randomInt(1, soundCount);
            Hub.getInstance().getAnnouncementsJukebox().playSoundForShow(new MallMedia("http://cdn.redstone.tech/Rushmeadfiles/mallmc/mcjukebox/hub/annoucements/" + name.toLowerCase() + "/" + name.toLowerCase() + soundNumber + ".mp3"));
        }
        for (AnnouncementMessage announcementMessage : annoucemtMessages) {
            for (MallPlayer mallPlayer : MallPlayer.getPlayers().values()) {
                List<String> messages = MallCore.getInstance().getLanguageMap().get(mallPlayer.getLanguageIdentifier()).getStrings("hub.announcements." + name + ".messages." + announcementMessage.getName());
                switch (announcementMessage.getMessageType()) {

                    case NORMAL:
                        messages.forEach(message -> Messaging.sendRawMessage(mallPlayer, message));
                        break;
                    case CLICLABLE:
                        messages.forEach(message -> Messaging.sendRawClickableMessage(Bukkit.getPlayer(mallPlayer.getUuid()), message, announcementMessage.getClickCommand()));
                        break;
                }
            }

        }
    }

    public String getName() {
        return name;
    }
}
