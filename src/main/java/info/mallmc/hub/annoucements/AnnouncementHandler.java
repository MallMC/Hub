package info.mallmc.hub.annoucements;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import info.mallmc.hub.Hub;
import org.bukkit.Bukkit;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AnnouncementHandler
{
    private List<Announcement> anncoucemnets;

    private int count;

    public AnnouncementHandler()
    {
        anncoucemnets = new ArrayList<>();
        loadAnnoucements();
        Bukkit.getScheduler().scheduleSyncDelayedTask(Hub.getInstance(), () -> anncoucemnets.get(count).triggerAnnouncement(), 20*60*5);
    }

    public void loadAnnoucements() {
        for(Announcement anncoucemnet: this.anncoucemnets) {
            anncoucemnets.remove(anncoucemnet);
        }
        try {
            JsonReader reader = new JsonReader(new FileReader(Hub.getInstance().getDataFolder() + "/announcements.json"));
            Gson gson = new Gson();
            Announcements announcements = gson.fromJson(reader, Announcements.class);
            for(Announcement anncoucemnet: announcements.getAnnouncements()) {
                this.anncoucemnets.add(anncoucemnet);
            }
            this.count = 0;
            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public List<Announcement> getAnncoucemnets() {
        return anncoucemnets;
    }

    private class Announcements {
        List<Announcement> announcements;

        public List<Announcement> getAnnouncements() {
            return announcements;
        }
    }

}
