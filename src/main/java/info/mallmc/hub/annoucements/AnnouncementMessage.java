package info.mallmc.hub.annoucements;

import com.google.gson.annotations.SerializedName;
import org.bukkit.craftbukkit.libs.jline.internal.Nullable;

public class AnnouncementMessage
{
    private String name;
    private AnnouncementMessageType messageType;
    @Nullable
    private String clickCommand;


    public String getName() {
        return name;
    }

    public AnnouncementMessageType getMessageType() {
        return messageType;
    }

    public String getClickCommand() {
        return clickCommand;
    }

    public enum AnnouncementMessageType
    {
        @SerializedName("normal")
        NORMAL,
        @SerializedName("clickable")
        CLICLABLE,
    }
}
