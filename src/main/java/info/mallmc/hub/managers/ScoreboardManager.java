package info.mallmc.hub.managers;

import info.mallmc.core.MallCore;
import info.mallmc.core.api.player.EnumLanguageIdentifier;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.events.EventHandler;
import info.mallmc.core.events.EventType;
import info.mallmc.framework.util.Messaging;
import info.mallmc.framework.util.SimpleScoreboard;
import info.mallmc.hub.util.HubStringUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ScoreboardManager
{

    private HashMap<EnumLanguageIdentifier, List<String>> scoreboardItems;

    public ScoreboardManager()
    {
        scoreboardItems = new HashMap<>();
        setScorebaordItems();
        EventHandler.registerEvent(EventType.LANG_RELOAD, event -> setScorebaordItems());
    }

    public void setScorebaordItems() {
        scoreboardItems.clear();
        for (EnumLanguageIdentifier enumLanguageIdentifier : EnumLanguageIdentifier.values()) {
            List<String> items = new ArrayList<>();
            for (String s : MallCore.getInstance().getLanguageMap().get(enumLanguageIdentifier).getStrings("hub.scoreboard.items")) {
                items.add(s);
            }
            this.scoreboardItems.put(enumLanguageIdentifier, items);
        }
        for(MallPlayer mallPlayer: MallPlayer.getPlayers().values()) {
            displayLobbyScorboard(mallPlayer);
        }
    }


    public void displayLobbyScorboard(MallPlayer mallPlayer) {
        Player player = Bukkit.getPlayer(mallPlayer.getUuid());
        if (SimpleScoreboard.getManager().getScoreboard(player) != null) {
            SimpleScoreboard.getManager().getScoreboard(player).unregister();
        }
        SimpleScoreboard.getManager().setupBoard(player);

        String gotString = Messaging.getMessage(mallPlayer.getLanguageIdentifier(), "hub.scoreboard.title");
        String replacedMsg = HubStringUtils.replacePlayerInfoInString(gotString, mallPlayer);
        SimpleScoreboard.getManager().getScoreboard(player).setHeader(replacedMsg);
        List<String> itemsToDisplay = new ArrayList<>();
        for (String string : scoreboardItems.get(mallPlayer.getLanguageIdentifier())) {
            itemsToDisplay.add(HubStringUtils.replacePlayerInfoInString(string, mallPlayer));
        }
        SimpleScoreboard.getManager().getScoreboard(player).setScores(itemsToDisplay);
        SimpleScoreboard.getManager().getScoreboard(player).update();

    }

}
