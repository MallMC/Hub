package info.mallmc.hub.managers;

import info.mallmc.core.MallCore;
import info.mallmc.core.api.player.EnumLanguageIdentifier;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.framework.api.MallBossBar;
import info.mallmc.framework.events.custom.OneSecondEvent;
import info.mallmc.hub.Hub;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BossBarManager implements Listener {
    private HashMap<EnumLanguageIdentifier, List<String>> bossBarMsgs;
    private HashMap<EnumLanguageIdentifier, MallBossBar> bossBars;
    private HashMap<EnumLanguageIdentifier, Integer> spotCount;

    public BossBarManager() {
        bossBarMsgs = new HashMap<>();
        bossBars = new HashMap<>();
        spotCount = new HashMap<>();
        setUpBossBars();
        Hub.getInstance().getPluginManager().registerEvents(this, Hub.getInstance());
    }

    public void setUpBossBars() {
        for (EnumLanguageIdentifier enumLanguageIdentifier : EnumLanguageIdentifier.values()) {
            List<String> items = new ArrayList<>();
            for (String s : MallCore.getInstance().getLanguageMap().get(enumLanguageIdentifier).getStrings("hub.bossbar.items")) {
                items.add(s);
            }
            this.bossBarMsgs.put(enumLanguageIdentifier, items);
            MallBossBar bossBar = new MallBossBar("", BarColor.BLUE, BarStyle.SOLID);
            bossBar.setVisible(true);
            this.bossBars.put(enumLanguageIdentifier, bossBar);
            this.spotCount.put(enumLanguageIdentifier, 0);
        }
    }

    public void addPlayer(MallPlayer mallPlayer) {
        this.bossBars.get(mallPlayer.getLanguageIdentifier()).addPlayer(mallPlayer);
    }

    public void removePlayer(MallPlayer mallPlayer) {
        this.bossBars.get(mallPlayer.getLanguageIdentifier()).removePlayer(mallPlayer);
    }

    @EventHandler
    public void onSec(OneSecondEvent event) {
        for (MallBossBar mallBossBar : bossBars.values()) {
            for (EnumLanguageIdentifier enumLanguageIdentifier : this.bossBarMsgs.keySet()) {
                if (this.bossBars.get(enumLanguageIdentifier).equals(mallBossBar)) {
                    mallBossBar.setTitle(this.bossBarMsgs.get(enumLanguageIdentifier).get(this.spotCount.get(enumLanguageIdentifier)));
                    int count = this.spotCount.get(enumLanguageIdentifier);
                    this.spotCount.remove(enumLanguageIdentifier);
                    if (count >= this.bossBarMsgs.get(enumLanguageIdentifier).size()) {
                        this.spotCount.put(enumLanguageIdentifier, 0);
                    } else {
                        this.spotCount.put(enumLanguageIdentifier, ++count);
                    }

                }
            }
        }
    }
}

